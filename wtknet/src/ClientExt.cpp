#ifndef CLIENTEXT_CPP 
#define CLIENTEXT_CPP 

#include "ClientExt.hpp" 

namespace wtk {
	namespace net {

ClientExt::ClientExt(size_t bsize) : Client(bsize) {
}

ClientExt::~ClientExt(void) {
}
		
bool ClientExt::SendRecv(const char* query, std::string *reply, 
		std::string hdr, std::string trl, float waitms) {

	int bytes = Client::Send(query);
	if(bytes == TR_BYTES_ERROR || bytes == TR_BYTES_NONE)
		return false;
	
	wtk::core::Streamer* stream;
	wtk::core::TimeValue tic;
	wtk::core::Time::Tic(&tic);
	while(true) {
		Socket::_semsocket.Wait();
		stream = Socket::GetStream(Socket::_socket->fd);
		if(stream->Extract(reply, hdr, trl, wtk::core::Streamer::Reverse)) {
			Socket::_semsocket.Post();
			return true;
		}
		Socket::_semsocket.Post();

		if(wtk::core::Time::Toc(&tic) > waitms)
			return false;
		wtk::core::Time::Sleep(ASYNC_WAIT_RECV);
	}
	return false;
}

bool ClientExt::SendRecv(const std::string& query, std::string *reply, 
		std::string hdr, std::string trl, float waitms) {
	return this->SendRecv(query.c_str(), reply, hdr, trl, waitms);
}

	}
}

#endif
