#ifndef SOCKET_CPP
#define SOCKET_CPP

#include "Socket.hpp"

namespace wtk {
	namespace net {

Socket::Socket(size_t bsize) {
	this->_semsocket.Wait();
	this->_socket = new tr_socket;
	tr_init_socket(this->_socket, bsize, 32);
	this->_semsocket.Post();
}

Socket::~Socket(void) {
	this->_semsocket.Wait();
	if(tr_check(this->_socket))
		tr_close(this->_socket);
	tr_free(this->_socket);
	delete this->_socket;
	this->_semsocket.Post();
}
		
void Socket::AddSend(const unsigned int bytes) {
	this->_bytesSend += bytes;
}

void Socket::AddRecv(const unsigned int bytes) {
	this->_bytesRecv += bytes;
}

long unsigned int Socket::GetSend(void) {
	long unsigned int bytes = this->_bytesSend;
	return bytes;
}

long unsigned int Socket::GetRecv(void) {
	long unsigned int bytes = this->_bytesRecv;
	return bytes;
}

bool Socket::IsConnected(void) {
	this->_semsocket.Wait();
	bool status = (tr_connected(this->_socket) == 0);
	this->_semsocket.Post();
	return status;
}

ssize_t Socket::Recv(tr_socket* peer) {
	int status = 0;
	status = tr_check(peer);
	if(status <= 0) {
		CcLogFatal("Socket is broken");
		return status;
	}

	ssize_t bytes = tr_recv(peer); 
	if(bytes == TR_BYTES_ERROR || bytes == TR_BYTES_NONE) {
		return bytes;
	}
	this->AddRecv(bytes);
	
	wtk::core::Streamer* stream = Socket::GetStream(peer->fd);
	if(stream)
		stream->Append((char*)peer->buffer, bytes);
	else
		CcLogWarningS("Stream not found for socket " << peer->fd);
		
	return bytes;
}

ssize_t Socket::Send(tr_socket* peer, const void* message, size_t size) {
	int bytes = TR_BYTES_NONE;

	bytes = tr_sendb(peer, (void*)message, size);
	if(bytes == TR_BYTES_ERROR || bytes == TR_BYTES_NONE)
		return bytes;
	
	this->AddSend(bytes);
	Socket::iOnSend.Execute(this);
	return bytes;
}
		
bool Socket::Close(void) {
	int fd_old = 0, fd_new = 0;
	
	fd_old = tr_check(this->_socket);
	if(fd_old > 0)
		fd_new = tr_close(Socket::_socket);
	if(fd_new > 0)
		CcLogErrorS("Cannot close socket " << this->_socket->fd);
	
	return(fd_new == 0 && fd_old > 0);
}

Address Socket::GetLocal(void) {
	this->_semsocket.Wait();
	Endpoint ep;
	ep.Set(&Socket::_socket->local);
	Address addr = ep.GetAddress();
	this->_semsocket.Post();
	return addr;
}

Address Socket::GetRemote(void) {
	this->_semsocket.Wait();
	Endpoint ep;
	ep.Set(&Socket::_socket->remote);
	Address addr = ep.GetAddress();
	this->_semsocket.Post();
	return addr;
}
		
int Socket::GetFID(void) {
	this->_semsocket.Wait();
	int fid = this->_socket->fd;
	this->_semsocket.Post();
	return fid;
}
		
wtk::core::Streamer* Socket::GetStream(int fid) {
	return this->_streams[fid];
}

bool Socket::AddStream(int fid) {
	this->_streams[fid] = new wtk::core::Streamer;
	return true;
}

bool Socket::RemStream(int fid) {
	wtk::core::Streamer* stream;
	stream = this->_streams[fid];
	this->_streams[fid] = NULL;
	this->_streams.erase(fid);
	
	if(stream != NULL)
		delete stream;
	return true;
}

bool Socket::HasStream(int fid) {
	StreamerMapIt it = this->_streams.find(fid);
	return(it != this->_streams.end());
}

tr_socket* Socket::GetPeer(int fid) {
	return this->_peers[fid];
}

bool Socket::AddPeer(tr_socket* peer) {
	this->_peers[peer->fd] = peer;
	return true;
}

bool Socket::RemPeer(int fid) {
	tr_socket* peer;
	peer = this->_peers[fid];
	this->_peers[fid] = NULL;
	this->_peers.erase(fid);
	
	if(peer != NULL)
		delete peer;
	return true;
}

bool Socket::HasPeer(int fid) {
	SocketMapIt it = this->_peers.find(fid);
	return(it != this->_peers.end());
}

Address Socket::GetAddress(int fid) {
	Endpoint ep;
	tr_socket* sock = GetPeer(fid);
	ep.Set(&sock->remote);
	return ep.GetAddress();
}
		
tr_socket* Socket::GetPeer(Address addr) {
	Endpoint cache;
	cache.SetAddress(addr);
	Ip ip = cache.GetIp();
	PortUInt port = cache.GetPortUInt();

	SocketMapIt pit;
	tr_socket* peer = NULL;
	for(pit = this->_peers.begin(); pit != this->_peers.end(); pit++) {
		peer = pit->second;
		if(peer->remote.port == port) {
			if(strcmp(peer->remote.address, ip.c_str()) == 0)
				return peer;
		}
	}
	return NULL;
}

void Socket::Dump(void) {
	Address addr;
	StreamerMapIt sit;
	SocketMapIt pit;
	
	printf("[Socket::Dump] Internal table:\n");
	printf("FID  Remote Address         Peer\n");
	int fid;
	for(sit = this->_streams.begin(); sit != this->_streams.end(); sit++) {
		fid = sit->first;
		addr = this->GetAddress(fid);
		pit = this->_peers.find(fid);
		
		printf("%4.4d ", fid);

		if(addr.empty() == false)
			printf("%-22.22s ", addr.c_str());
		else
			printf("%-22.22s ", "N/A");
		
		if(pit != this->_peers.end())
			printf("%p", pit->second);
		else
			printf("%s ", "NULL");

		printf("\n");
	}

}

Address Socket::Lookup(std::string name) {
	char address[tr_getaddrlen()];
	tr_resolve(name.c_str(), address);
	return Address(address);
}
		
unsigned int Socket::GetPeers(void) {
	this->_semsocket.Wait();
	unsigned int peers = this->_peers.size();
	this->_semsocket.Post();
	return peers;
}

	}
}

#endif
