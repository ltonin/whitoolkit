#ifndef CLIENTEXT_HPP 
#define CLIENTEXT_HPP 

#include <wtkcore/Time.hpp>
#include <wtkcore/Streamer.hpp>

#include "Client.hpp"

namespace wtk {
	namespace net {

class ClientExt : public Client {
	public:
		ClientExt(size_t bsize = CORE_1MB);
		virtual ~ClientExt(void);
		
		bool SendRecv(const char* query, std::string *reply, 
				std::string hdr, std::string trl, float waitms = -1);
		bool SendRecv(const std::string& query, std::string *reply, 
				std::string hdr, std::string trl, float waitms = -1);
};

	}
}

#endif
