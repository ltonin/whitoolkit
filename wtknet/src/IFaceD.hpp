#ifndef IFACE_D_HPP
#define IFACE_D_HPP

#include <tobicore/TCLanguage.hpp>
#include <tobicore/TCException.hpp>
#include <tobiid/IDMessage.hpp>
#include <tobiid/IDSerializerRapid.hpp>

#include <cnbiloop/ClLoop.hpp>

#include <wtkprotocol/TaskSet.hpp>
#include "ClTobiId.hpp"

namespace wtk {
	namespace net {

class IFaceD : public cnbi::ClTobiId {


	public:
		IFaceD(int mode, float waitms = 3.0f);
		~IFaceD(void);
		
		void Set(std::string mdesc, IDFtype ftype = IDMessage::FamilyBiosig);
		bool TryAttach(const std::string& name, bool blocking = false);
		void Dump(void);

		// As Sender
		void Send(unsigned int value, int blockidx = TCBlock::BlockIdxUnset, int* fixd = NULL);

		// As Receiver
		bool HasEvent(void);
		unsigned int GetEvent(void);


	public:
		const static int AsSender 	= 0;
		const static int AsReceiver 	= 1;
		const static int AsBoth 	= 2;

	protected:
		bool 			isset_;
		IDMessage*		idmsg_ 	  = NULL;
		IDSerializerRapid* 	idserial_ = NULL;

		std::string		idmdesc_;
		IDFtype 		idftype_;
};

	}
}


#endif
