#ifndef CNBILOOP_TOBIID_HPP 
#define CNBILOOP_TOBIID_HPP 

#include <list>

#include <cnbiloop/ClLoop.hpp>

#include <tobicore/TCException.hpp>
#include <tobicore/TCLanguage.hpp>
#include <tobiid/IDMessage.hpp>
#include <tobiid/IDSerializerRapid.hpp>

#include <wtkcore/Callback.hpp>
#include <wtkcore/Proxy.hpp>
#include <wtkcore/Streamer.hpp>
#include <wtkcore/Semaphore.hpp>

#include "SocketProxy.hpp"
#include "ClientExt.hpp"

using namespace wtk::net;

namespace wtk {
	namespace net {
		namespace cnbi {

class ClTobiId : public SocketProxy {
	public:
		ClTobiId(int mode = ClTobiId::SetOnly, float waitms = 3.00);
		virtual ~ClTobiId(void);
		bool Attach(const std::string& name = "/bus");
		bool Detach(void);
		bool IsAttached(void);
		bool GetMessage(IDSerializerRapid* serializer);
		bool SetMessage(IDSerializerRapid* serializer, 
				int blockidx = TCBlock::BlockIdxUnset, int* fixd = NULL);
		int Count(void);
		int Clear(void);
	protected:
		void HandleConnect(Socket* caller);
		void HandleDisconnect(Socket* caller);
		void HandleRecv(Socket* caller, wtk::core::Streamer* stream);

	public:
		static const int SetOnly = 0;
		static const int GetOnly = 1;
		static const int SetGet = 2;
		wtk::core::Callback0<wtk::core::Proxy> iOnAttach;
		wtk::core::Callback0<wtk::core::Proxy> iOnDetach;
		wtk::core::Callback0<wtk::core::Proxy> iOnHasMessage;

	protected:
		ClientExt* _client;
		std::string _name;
		std::string _buffer;
		wtk::core::Semaphore _sembuffer;
		
		wtk::core::Semaphore _semqueue;
		std::list<std::string> _queue;
		int _mode;
		TCLanguage _tclang;
		float _waitms;
};
	
		}
	}
}

#endif
