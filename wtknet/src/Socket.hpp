#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <map>
#include <stdio.h>
#include <string>

#include <transport/tr_net.h>
#include <transport/tr_names.h>
#include <transport/tr_types.h>

#include <cnbicore/CcLog.hpp>

#include <wtkcore/Callback.hpp>
#include <wtkcore/Flags.hpp>
#include <wtkcore/Object.hpp>
#include <wtkcore/Semaphore.hpp>
#include <wtkcore/Streamer.hpp>
#include <wtkcore/ThreadSafe.hpp>

#include "NetworkTypes.hpp"
#include "Endpoint.hpp"

namespace wtk {
	namespace net {

class SocketProxy;

class Socket : public wtk::core::Object {
	public:
		Socket(size_t bsize);
		virtual ~Socket(void);
		virtual Address GetLocal(void);
		virtual Address GetRemote(void);
		long unsigned int GetSend(void);
		long unsigned int GetRecv(void);
		unsigned int GetPeers(void);
		int GetFID(void);
		virtual bool IsConnected(void);
		void Dump(void);
		static Address Lookup(std::string name);
	protected:
		virtual ssize_t Send(const char* message) = 0;
		virtual ssize_t Send(const std::string& message) = 0;
		virtual ssize_t Send(const void* message, size_t size) = 0;
		virtual ssize_t Send(tr_socket* peer, const void* message, size_t size); 
		virtual ssize_t Recv(tr_socket* peer); 
		virtual bool Open(int protocol) = 0;
		virtual bool Close(void);
		void AddSend(const unsigned int bytes);	
		void AddRecv(const unsigned int bytes);
		
		wtk::core::Streamer* GetStream(int fid);
		bool AddStream(int fid);
		bool RemStream(int fid);
		bool HasStream(int fid);
		
		tr_socket* GetPeer(int fid);
		bool AddPeer(tr_socket* peer);
		bool RemPeer(int fid);
		bool HasPeer(int fid);
		
		tr_socket* GetPeer(Address addr);
		Address GetAddress(int fid);

	protected:
		tr_socket* _socket;
		wtk::core::Semaphore _semsocket;
		wtk::net::StreamerMap _streams;
		wtk::net::SocketMap _peers;
	private:
		long unsigned int _bytesRecv;
		long unsigned int _bytesSend;
	public:
		const static int TCP = 0;
		const static int UDP = 1;
		wtk::core::Callback2<SocketProxy, Socket*, wtk::core::Streamer*> iOnRecv;
	protected:
		wtk::core::Callback1<SocketProxy, Socket*> iOnSend;
};

	}
}

#endif
