#ifndef IFACE_D_CPP
#define IFACE_D_CPP

#include "IFaceD.hpp"

namespace wtk {
	namespace net {

IFaceD::IFaceD(int mode, float waitms) { 
	switch(mode) { 
		case IFaceD::AsReceiver:
			this->_mode = wtk::net::cnbi::ClTobiId::GetOnly;
			break;
		case IFaceD::AsSender:
			this->_mode = wtk::net::cnbi::ClTobiId::SetOnly;
			break;
		case IFaceD::AsBoth:
			this->_mode = wtk::net::cnbi::ClTobiId::SetGet;
			break;
		default:
			throw std::runtime_error("Interface D mode not recognized");
	}

	ClLoop::Instance();
	this->_client = NULL;
	this->_waitms = waitms;

	this->idmsg_    = new IDMessage;
	this->idserial_ = new IDSerializerRapid(this->idmsg_);
	this->isset_    = false;
}

IFaceD::~IFaceD(void) {
	ClLoop::Release();
}
void IFaceD::Set(std::string mdesc, IDFtype ftype) {
	this->idmsg_->SetDescription(mdesc);
	this->idmsg_->SetFamilyType(ftype);
	this->isset_    = true;
}

bool IFaceD::TryAttach(const std::string& stream, bool blocking) {
	std::string sblock = blocking ? "(blocking)" : "(no blocking)";	
	bool atbegin = true;

	if(this->IsAttached() == false) {
		CcLogInfoS("Try to attach Id to " + stream + " " + sblock);
		atbegin = false;
	}

	while(this->IsAttached() == false) {
		this->Attach(stream);

		if(blocking == false)
			break;

		wtk::core::Time::Sleep(1000);
	}

	if(atbegin == false) {
		CcLogInfoS("Id attached to " + stream);
	}

	return this->IsAttached();
}



void IFaceD::Send(unsigned int value, int blockidx, int* fixd) {
	if(this->_mode == wtk::net::cnbi::ClTobiId::GetOnly) 
		throw std::runtime_error("Interface D is set as Receiver. Cannot send event.");
	
	if(this->isset_ == false) 
		throw std::runtime_error("Interface D is not set");

	if(this->IsAttached() == true) {
		this->idmsg_->SetEvent(value);
		this->SetMessage(this->idserial_, blockidx, fixd);
	}
}

bool IFaceD::HasEvent(void) {
	bool retcode = false;
	if(this->_mode == wtk::net::cnbi::ClTobiId::SetOnly) 
		throw std::runtime_error("Interface D is set as Sender. Cannot receive event.");
	
	if(this->isset_ == false) 
		throw std::runtime_error("Interface D is not set");

	if(this->IsAttached() == true) {
		retcode = this->GetMessage(this->idserial_);
	}

	return retcode;
}


unsigned int IFaceD::GetEvent(void) {
	if(this->_mode == wtk::net::cnbi::ClTobiId::SetOnly) 
		throw std::runtime_error("Interface D is set as Sender. Cannot receive event.");
	
	if(this->isset_ == false) 
		throw std::runtime_error("Interface D is not set");

	return this->idmsg_->GetEvent();
}

void IFaceD::Dump(void) {
	this->idmsg_->Dump();
}

	}
}

#endif
