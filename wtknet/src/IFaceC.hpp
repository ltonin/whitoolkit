#ifndef IFACE_C_HPP
#define IFACE_C_HPP

#include <tobicore/TCException.hpp>
#include <tobiic/ICMessage.hpp>
#include <tobiic/ICSerializerRapid.hpp>

#include <cnbiloop/ClLoop.hpp>

#include <wtkprotocol/TaskSet.hpp>

#include "ClTobiIc.hpp"

namespace wtk {
	namespace net {

class IFaceC : public cnbi::ClTobiIc {


	public:
		IFaceC(int mode, bool nscache = true);
		~IFaceC(void);
		
		void Set(std::string cname, std::string cdesc, ICVtype vtype = ICClassifier::ValueProb, ICLtype ltype = ICClassifier::LabelBiosig);
		bool TryAttach(const std::string& name, bool blocking = false);
		void Dump(void);

		// As Sender
		void CreateMessage(wtk::protocol::TaskSet* taskset);
		void SetValue(std::string label, float value);
		void Send(unsigned int idx);

		// As Receiver
		ICMessage* GetMessage(bool blocking = true);
		bool SyncMessage(ICMessage* icmsg, int fidx);
		bool ValidateMessage(ICMessage* message, wtk::protocol::TaskSet* taskset);


	public:
		const static int AsSender 	= 0;
		const static int AsReceiver 	= 1;

	protected:
		bool 			isset_;
		ICMessage*		icmsg_ 	  = NULL;
		ICSerializerRapid* 	icserial_ = NULL;

		ICClassifier*		icc_      = NULL;
		std::string		iccname_;
		std::string 		iccdesc_;
		ICVtype			iccvtype_;
		ICLtype 		iccltype_;



};


	}
}

#endif
