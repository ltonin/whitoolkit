#ifndef CNBILOOP_TOBIID_CPP 
#define CNBILOOP_TOBIID_CPP 

#include "ClTobiId.hpp" 

namespace wtk {
	namespace net {
		namespace cnbi {

ClTobiId::ClTobiId(int mode, float waitms) {
	ClLoop::Instance();
	this->_client = NULL;
	this->_mode = mode;
	this->_waitms = waitms;
}

ClTobiId::~ClTobiId(void) {
	ClLoop::Release();
}

bool ClTobiId::Attach(const std::string& name) {
	if(this->_client != NULL)
		return false;
	
	this->_name.assign(name);
	
	this->_client = new ClientExt;
	CB_Socket(this->_client->iOnConnect, this, HandleConnect);
	CB_Socket(this->_client->iOnDisconnect, this, HandleDisconnect);
	CB_Socket(this->_client->iOnRecv, this, HandleRecv);

	if(ClLoop::Connect() == false) { 
		CcLogError("Cannot connect to loop");
		return false;
	}

	CcAddress address;
	if(ClLoop::nms.Query(this->_name, &address) != ClNmsLang::Successful) {
		CcLogErrorS("Cannot query " << this->_name);
		return false;
	}

	if(this->_client->Connect(address) == false) {
		CcLogErrorS("Cannot connect to " << address);
		return false;
	}
	

	return true;
}

bool ClTobiId::Detach(void) {
	if(this->_client == NULL)
		return true;

	if(this->_client->IsConnected())
		this->_client->Disconnect();
	delete(this->_client);
	this->_client = NULL;
	return true;
}

bool ClTobiId::IsAttached(void) {
	if(this->_client == NULL)
		return false;
	return this->_client->IsConnected();
}

void ClTobiId::HandleConnect(Socket* caller) {
	ClientExt* client = (ClientExt*)caller;
	CcLogDebugS("Connected TCP endpoint: " << client->GetRemote());
	this->iOnAttach.Execute();
}

void ClTobiId::HandleDisconnect(Socket* caller) {
	ClientExt* client = (ClientExt*)caller;
	CcLogDebugS("Disconnected TCP endpoint: " << client->GetRemote());
	this->iOnDetach.Execute();
}

void ClTobiId::HandleRecv(Socket* caller, wtk::core::Streamer* stream) {
	this->_semqueue.Wait();
	
	if(this->_mode != ClTobiId::SetOnly) {
		std::string buffer;
		while(stream->Extract(&buffer, "<tobiid", "/>") == true) {
			this->_queue.push_back(buffer);
			this->iOnHasMessage.Execute();
		}
	}
	this->_semqueue.Post();
}
		
bool ClTobiId::GetMessage(IDSerializerRapid* serializer) {
	if(this->_mode == ClTobiId::SetOnly) {
		CcLogError("iD interface configures as SetOnly");
		return false;
	}

	this->_semqueue.Wait();
	if(this->_queue.empty()) {
		this->_semqueue.Post();
		return false;
	}
	std::string buffer = this->_queue.front();
	serializer->Deserialize(&buffer);
	this->_queue.pop_front();
	this->_semqueue.Post();
	return true;
}
		
int ClTobiId::Count(void) {
	int count = 0;
	this->_semqueue.Wait();
	count = this->_queue.size();
	this->_semqueue.Post();
	return count;
}
		
int ClTobiId::Clear(void) {
	int count = 0;
	this->_semqueue.Wait();
	count = this->_queue.size();
	this->_queue.clear();
	this->_semqueue.Post();
	return count;
}
		
bool ClTobiId::SetMessage(IDSerializerRapid* serializer, int blockidx, int* fidx) {
	if(this->_mode == ClTobiId::GetOnly) {
		CcLogError("iD interface configures as GetOnly");
		return false;
	}
	
	// Assemble iD message and serialize it
	std::string buffer;
	serializer->message->absolute.Tic();
	serializer->message->SetBlockIdx(blockidx);
	serializer->Serialize(&buffer);
	
	// Prepare stuff for TCLanguage
	std::string reply;
	int component, status, tcfidx;

	if(!this->_client->SendRecv(buffer, &reply, "<tcstatus", "/>", this->_waitms)) {
		CcLogWarningS("TiD status message not received after " << this->_waitms << "ms");
		return false;
	}

	if(!this->_tclang.IsStatus(reply, &component, &status, &tcfidx)) {
		CcLogWarningS("TiD status message received but is not valid: " << reply);
		return false;
	}
	
	if(status != TCLanguage::Ready) {
		CcLogErrorS("TiD interface reports to be down");
		return false;
	}
	
	if(fidx != NULL) 
		*fidx = tcfidx;
	return true;
}

		}

	}
}

#endif
