#ifndef CLIENT_CPP 
#define CLIENT_CPP 

#include "Client.hpp" 

namespace wtk {
	namespace net {

Client::Client(size_t bsize) : Socket(bsize) {
}

Client::~Client(void) {
}
		
bool Client::Connect(Address address, int protocol) {
	Socket::_semsocket.Wait();
	if(wtk::core::Thread::IsRunning() == true) {
		CcLogWarning("Client thread already running");
		return true;
	}
		
	Socket::Close();
	if(this->Open(protocol) == false) {
		CcLogError("Cannot open socket");
		Socket::_semsocket.Post();
		return false;
	}
	
	Endpoint cache(address);
	
	int status = tr_connect(Socket::_socket, 
			cache.GetIp().c_str(), cache.GetPort().c_str());
	if(status != 0) {
		Socket::_semsocket.Post();
		CcLogError("Cannot connect socket");
		return false;
	}
	tr_set_nonblocking(Socket::_socket, 1);
	Socket::AddStream(Socket::_socket->fd);
	Socket::AddPeer(Socket::_socket);
	
	Endpoint local, remote;
	local.Set(&Socket::_socket->local);
	remote.Set(&Socket::_socket->remote);
	CcLogDebugS("Socket " << Socket::_socket->fd << " connected: " << 
			local.GetAddress() << " to " << remote.GetAddress());
	Socket::_semsocket.Post();

	wtk::core::Thread::Start();
	
	this->iOnConnect.Execute(this);
	return true;
}

bool Client::Disconnect(void) {
	if(this->IsConnected() == false) {
		CcLogWarning("Socket not connected");
		return false;
	}

	if(wtk::core::Thread::IsRunning() == true) {
		wtk::core::Thread::Stop();
		wtk::core::Thread::Join();
	}
	
	Socket::_semsocket.Wait();
	if(this->Close() == false) {
		Socket::_semsocket.Post();
		return false;
	}
	Socket::RemStream(Socket::_socket->fd);
	Socket::_semsocket.Post();
	
	this->iOnDisconnect.Execute(this);
	return true;
}

ssize_t Client::Send(const char* message) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, (void*)message,
			strlen(message));
	Socket::_semsocket.Post();
	
	return bytes;
}

ssize_t Client::Send(const std::string& message) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, (void*)message.c_str(),
			message.size());
	Socket::_semsocket.Post();
	
	return bytes;
}
		
ssize_t Client::Send(const void* message, size_t size) {
	Socket::_semsocket.Wait();
	ssize_t bytes = Socket::Send(Socket::_socket, message, size);
	Socket::_semsocket.Post();

	return bytes;
}
		
void Client::Main(void) {
	if(!wtk::core::Thread::IsRunning())
		return;
	
	struct timeval tv;
	fd_set readfds;
	FD_ZERO(&readfds);

	int status;
	bool received;
	wtk::core::Streamer* stream = Socket::GetStream(Socket::_socket->fd);
	while(wtk::core::Thread::IsRunning()) {
		FD_SET(Socket::_socket->fd, &readfds);
		tv.tv_sec = CORE_ASIO_SEC;
		tv.tv_usec = CORE_ASIO_USEC;
		
		received = false;

		status = select(Socket::_socket->fd + 1, &readfds, NULL, NULL, &tv);
		if(status == -1) {
			CcLogFatalS("Async I/O error: " << strerror(status));
			wtk::core::Thread::Stop();
		} else if(status > 0) {
			if(FD_ISSET(Socket::_socket->fd, &readfds)) {
				Socket::_semsocket.Wait();
				if(Socket::Recv(Socket::_socket) == TR_BYTES_ERROR) {
					wtk::core::Thread::Stop();
				} else {
					received = true;
				}
				Socket::_semsocket.Post();
			}
		}
		if(received) 
			Socket::iOnRecv.Execute(this, stream);
	}
	FD_ZERO(&readfds);
	
	// Edited by L.Tonin  <luca.tonin@dei.unipd.it> on 27/06/15 02:21:58
	// Double call to Disconnect. Here and from outside (especially in
	// ClTobiI*). This led to segmentation fault. I've removed from here to
	// keep the control from outside the classe
	//this->Disconnect();
	Socket::_semsocket.Wait();
	Socket::RemStream(Socket::_socket->fd);
	Socket::_semsocket.Post();
	this->iOnDisconnect.Execute(this);
}

bool Client::Open(int protocol) {
	int status = 0;
	switch(protocol) {
		case Socket::UDP:
			tr_udpclient(Socket::_socket);
			break;
		case Socket::TCP:
		default:
			tr_tcpclient(Socket::_socket);
			break;
	}
	status = tr_open(Socket::_socket);
	
	if(status == 0)	{
		CcLogDebugS("Socket " << Socket::_socket->fd << " opened as " <<
				(protocol == Socket::UDP ? "UDP" : "TCP"));
	} else {
		CcLogErrorS("Cannot open socket as " << 
				(protocol == Socket::UDP ? "UDP" : "TCP"));
	}
	return(status == 0);
}

	}
}
		
#endif
