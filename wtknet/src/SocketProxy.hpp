#ifndef SOCKETPROXY_HPP 
#define SOCKETPROXY_HPP 

#include <cnbicore/CcLog.hpp>
#include <wtkcore/Streamer.hpp>

#include "Socket.hpp"

namespace wtk {
	namespace net {

class SocketProxy {
	public:
		virtual void HandleBind(Socket* caller);
		virtual void HandleRelease(Socket* caller);
		virtual void HandleListen(Socket* caller);
		
		virtual void HandleAccept(Socket* caller);
		virtual void HandleDrop(Socket* caller);
		
		virtual void HandleAcceptPeer(Socket* caller, Address addr);
		virtual void HandleDropPeer(Socket* caller, Address addr);
		
		virtual void HandleSend(Socket* caller);
		virtual void HandleRecv(Socket* caller, wtk::core::Streamer* stream);
		
		virtual void HandleSendPeer(Socket* caller, Address addr);
		virtual void HandleRecvPeer(Socket* caller, Address addr, wtk::core::Streamer* stream);

		virtual void HandleAccept(Socket* caller, Socket* ep);
		virtual void HandleDrop(Socket* caller, Socket* ep);
		
		virtual void HandleConnect(Socket* caller);
		virtual void HandleDisconnect(Socket* caller);
};

	}
}

#define CB_Socket(sender_event, receiver_ptr, method) 		\
	sender_event.Register((wtk::net::SocketProxy*)receiver_ptr, 		\
			&wtk::net::SocketProxy::method);

#endif
