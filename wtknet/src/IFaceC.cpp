#ifndef IFACE_C_CPP
#define IFACE_C_CPP

#include "IFaceC.hpp"

namespace wtk {
	namespace net {

IFaceC::IFaceC(int mode, bool nscache) {
	switch(mode) { 
		case IFaceC::AsReceiver:
			this->_mode = wtk::net::cnbi::ClTobiIc::GetOnly;
			break;
		case IFaceC::AsSender:
			this->_mode = wtk::net::cnbi::ClTobiIc::SetOnly;
			break;
		default:
			throw std::runtime_error("Interface C mode not recognized");
	}

	this->_server   = NULL;
	this->_client   = NULL;
	this->_onwsname = false;
	this->_nscache  = nscache;

	this->icmsg_    = new ICMessage;
	this->icserial_ = new ICSerializerRapid(this->icmsg_);
	this->isset_    = false;

}

IFaceC::~IFaceC(void) {
	ClLoop::Release();
}

void IFaceC::Set(std::string cname, std::string cdesc, ICVtype vtype, ICLtype ltype) {
	this->iccname_  = cname;
	this->iccdesc_  = cdesc;	
	this->iccvtype_ = vtype;
	this->iccltype_ = ltype;
	this->isset_    = true;
}

bool IFaceC::TryAttach(const std::string& stream, bool blocking) {
	std::string sblock = blocking ? "(blocking)" : "(no blocking)";	
	bool atbegin = true;

	if(this->IsAttached() == false) {
		CcLogInfoS("Try to attach Ic to " + stream + " " + sblock);
		atbegin = false;
	}

	while(this->IsAttached() == false) {
		this->Attach(stream);

		if(blocking == false)
			break;

		wtk::core::Time::Sleep(1000);
	}

	if(atbegin == false)
		CcLogInfoS("Ic attached to " + stream);

	return this->IsAttached();
}

void IFaceC::CreateMessage(wtk::protocol::TaskSet* taskset) {

	if(this->_mode == wtk::net::cnbi::ClTobiIc::GetOnly) 
		throw std::runtime_error("Interface C is set as Receiver. Cannot create messages.");

	if(this->isset_ == false) 
		throw std::runtime_error("Interface C is not set");

	this->icc_   = new ICClassifier(this->iccname_, this->iccdesc_, ICClassifier::ValueProb, ICClassifier::LabelBiosig);
	for(auto it = taskset->Begin(); it != taskset->End(); it++) {
		ICClass* cclass = new ICClass(it->first, 0.0f);
		this->icc_->classes.Add(cclass);
	}
	
	this->icmsg_->classifiers.Add(this->icc_);
}

void IFaceC::SetValue(std::string label, float value) {
	if(this->isset_ == false) 
		throw std::runtime_error("Interface C is not set");

	this->icmsg_->SetValue(this->iccname_, label, value);
}

void IFaceC::Send(unsigned int idx) {
	if(this->_mode == wtk::net::cnbi::ClTobiIc::GetOnly) 
		throw std::runtime_error("Interface C is set as Receiver. Cannot send messages.");
	
	if(this->isset_ == false) 
		throw std::runtime_error("Interface C is not set");

	if(this->IsAttached() == true)
		SetMessage(this->icserial_, idx);
}

ICMessage* IFaceC::GetMessage(bool blocking) {
	if(this->_mode == wtk::net::cnbi::ClTobiIc::SetOnly) 
		throw std::runtime_error("Interface C is set as Sender. Cannot receive messages.");
	
	if(this->isset_ == false) 
		throw std::runtime_error("Interface C is not set");
	
	if (this->IsAttached() == true) {
		if(blocking == true)
			this->WaitMessage(this->icserial_);
		else 
			this->GetMessage(this->icserial_);

		return this->icserial_->message;
	}

	return NULL;
}

bool IFaceC::SyncMessage(ICMessage* icmsg, int fidx) {
	if(icmsg->GetBlockIdx() > fidx)
		return true;

	return false;
}

bool IFaceC::ValidateMessage(ICMessage* message, wtk::protocol::TaskSet* taskset) {
	bool retcode = true;
	ICClassifier* icc;	
	try {
		icc = message->GetClassifier(this->iccname_);
		for(auto it = icc->classes.Begin(); it != icc->classes.End(); it++) {
			retcode = retcode & taskset->Has(it->first);
			if (retcode == false) {
				throw std::runtime_error("Taskset does not have task " + it->first + " required by ICMessage");
				break;
			} else {
				CcLogInfoS("Task " << taskset->Get(it->first)->Get("name").AsString() << " (" << it->first << ") => Id: " <<
			   	    	   taskset->Get(it->first)->GetId());
			}

		}

	} catch (TCException& e) {
		throw std::runtime_error("Classifier " + this->iccname_ + " in ICMessage not found in taskset");
	}
	return retcode;
}

void IFaceC::Dump(void) {
	this->icmsg_->Dump();
}
	}
}



#endif
