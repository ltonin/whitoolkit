#ifndef SOCKETPROXY_CPP 
#define SOCKETPROXY_CPP 

#include "SocketProxy.hpp" 
#include "Server.hpp" 

namespace wtk {
	namespace net {

void SocketProxy::HandleBind(Socket* caller) {
	CcLogDebugS("Socket " << caller->GetFID() << " bound");
}

void SocketProxy::HandleRelease(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " released");
}

void SocketProxy::HandleListen(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " listening");
}

void SocketProxy::HandleAccept(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " accepted");
}

void SocketProxy::HandleDrop(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " dropped");
}

void SocketProxy::HandleAcceptPeer(Socket* caller, Address addr) { 
	CcLogDebugS("Socket " << caller->GetFID() << " accepted " << addr);
}

void SocketProxy::HandleDropPeer(Socket* caller, Address addr) { 
	CcLogDebugS("Socket " << caller->GetFID() << " dropped " << addr);
}

void SocketProxy::HandleSend(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " sent data");
}

void SocketProxy::HandleRecv(Socket* caller, wtk::core::Streamer* stream) { 
	CcLogDebugS("Socket " << caller->GetFID() << " received data");
}

void SocketProxy::HandleSendPeer(Socket* caller, Address addr) { 
	CcLogDebugS("Socket " << caller->GetFID() << " sent to " << addr);
}

void SocketProxy::HandleRecvPeer(Socket* caller, Address addr, 
		wtk::core::Streamer* stream) { 
	CcLogDebugS("Socket " << caller->GetFID() << " received from " << addr);
}

void SocketProxy::HandleAccept(Socket* caller, Socket* ep) { 
	CcLogDebugS("Socket " << caller->GetFID() << " accepted " << ep->GetFID());
}

void SocketProxy::HandleDrop(Socket* caller, Socket* ep) { 
	CcLogDebugS("Socket " << caller->GetFID() << " dropped " << ep->GetFID());
}

void SocketProxy::HandleConnect(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " connected");
}

void SocketProxy::HandleDisconnect(Socket* caller) { 
	CcLogDebugS("Socket " << caller->GetFID() << " disconnected");
}

	}
}

#endif
