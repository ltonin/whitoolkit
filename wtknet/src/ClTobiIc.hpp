#ifndef CNBILOOP_TOBIIC_HPP 
#define CNBILOOP_TOBIIC_HPP 

#include <cnbiloop/ClLoop.hpp>
#include <tobicore/TCException.hpp>
#include <tobiic/ICMessage.hpp>
#include <tobiic/ICSerializerRapid.hpp>

#include <wtkcore/Callback.hpp>
#include <wtkcore/Proxy.hpp>
#include <wtkcore/Streamer.hpp>
#include <wtkcore/Semaphore.hpp>

#include "SocketProxy.hpp"
#include "Server.hpp"
#include "Client.hpp"

using namespace wtk::net;

namespace wtk {
	namespace net {
		namespace cnbi {

class ClTobiIc : public SocketProxy {
	public:
		ClTobiIc(int mode = ClTobiIc::GetOnly, bool nscache = true);
		virtual ~ClTobiIc(void);
		bool Attach(const Port port, const std::string& name);
		bool Attach(const std::string& name);
		bool Detach(void);
		bool IsAttached(void);
		int WaitMessage(ICSerializerRapid* serializer);
		int GetMessage(ICSerializerRapid* serializer);
		int SetMessage(ICSerializerRapid* serializer, 
				int blockidx = TCBlock::BlockIdxUnset);
		unsigned int GetPeers(void);
	protected:
		int Deserialize(ICSerializerRapid* serializer);
		void HandleAccept(Socket* caller);
		void HandleDrop(Socket* caller);
		void HandleRecvPeer(Socket* caller, Address addr, wtk::core::Streamer* stream);

	public:
		const static int WrongMode = -2;
		const static int Detached = -1;
		const static int HasMessage = 1;
		const static int MessageSet = 2;
		const static int NoMessage = 0;
		static const int SetOnly = 0;
		static const int GetOnly = 1;
		wtk::core::Callback0<wtk::core::Proxy> iOnAttach;
		wtk::core::Callback0<wtk::core::Proxy> iOnDetach;
		wtk::core::Callback0<wtk::core::Proxy> iOnHasMessage;
		wtk::core::Callback0<wtk::core::Proxy> iOnAccept;
		wtk::core::Callback0<wtk::core::Proxy> iOnDrop;

	protected:
		Client* _client;
		Server* _server;
		Address _address;
		bool _nscache;
		std::string _name;
		bool _onwsname;
		int _mode;
		std::string _buffer;
		wtk::core::Semaphore _sembuffer;
		wtk::core::Semaphore _hasmessage;
};
		}
	}
}

#endif

