#ifndef CLIENT_HPP 
#define CLIENT_HPP 

#include <string>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <cnbicore/CcLog.hpp>
#include <transport/tr_net.h>
#include <transport/tr_tcp.h>
#include <transport/tr_udp.h>

#include <wtkcore/Thread.hpp>
#include <wtkcore/Time.hpp>

#include "Endpoint.hpp"
#include "Socket.hpp"

namespace wtk {
	namespace net {

class Client : public Socket, protected wtk::core::Thread {
	public:
		Client(size_t bsize = CORE_1MB);
		virtual ~Client(void);
		bool Connect(Address address, int protocol = Socket::TCP);
		bool Disconnect(void);
		ssize_t Send(const char* message);
		ssize_t Send(const std::string& message);
		ssize_t Send(const void* message, size_t size);

	private:
		void Main(void);
		bool Open(int protocol);

	public:
		wtk::core::Callback1<SocketProxy, Socket*> iOnConnect;
		wtk::core::Callback1<SocketProxy, Socket*> iOnDisconnect;
};

	}
}


#endif
