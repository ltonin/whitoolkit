#ifndef ENDPOINT_HPP
#define ENDPOINT_HPP

#include <string>
#include <sstream>
#include <transport/tr_net.h>
#include <transport/tr_names.h>
#include <wtkcore/Object.hpp>
#include <wtkcore/Exception.hpp>

#include "NetworkTypes.hpp"

namespace wtk {
	namespace net {

class Endpoint : public wtk::core::Object {
	public:
		Endpoint(void);
		Endpoint(const Ip ip, const Port port);
		Endpoint(const Ip ip, const PortUInt port);
		Endpoint(const Address address);
		virtual ~Endpoint(void);

		virtual void Clear(void);

		virtual void Set(const tr_host *host, bool fixlocal = false);
		
		virtual void SetIp(const Ip ip);
		virtual Ip GetIp(void) const;

		virtual void SetPortUInt(const unsigned int port);
		virtual unsigned int GetPortUInt(void) const;
		
		virtual void SetPort(const Port port);
		virtual Port GetPort(void) const;
		
		virtual void SetAddress(const Address address);
		virtual Address GetAddress(void) const;
	private:
		virtual void MakeAddress(const Ip ip, const Port port);
		virtual void MakeAddress(const Ip ip, const unsigned int port);
		virtual void MakeAddress(const tr_host *host);
		virtual void DecomposeAddress(const Address address);
	public:
		const static Hostname HostnameUnset;
		const static Port PortUnset;
		const static PortUInt PortUintUnset;
		const static Ip IpUnset;
		const static Address AddressUnset;
	private:
		Ip _ip;
		Address _address;
		Port _port;
		unsigned int _iport;
};

	}
}
#endif
