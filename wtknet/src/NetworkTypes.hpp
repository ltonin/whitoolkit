#ifndef NETWORKTYPES_HPP
#define NETWORKTYPES_HPP

#include <map>
#include <string>

#include <transport/tr_types.h>

#include <wtkcore/Streamer.hpp>

namespace wtk {
	namespace net {

typedef std::string  Ip;
typedef std::string  Port;
typedef unsigned int PortUInt;
typedef std::string  Address;
typedef std::string  Hostname;

typedef std::map<int, wtk::core::Streamer*> StreamerMap;
typedef std::map<int, wtk::core::Streamer*>::iterator StreamerMapIt;
typedef std::map<int, wtk::core::Streamer*>::const_iterator StreamerMapCIt;

typedef std::map<int, tr_socket*> SocketMap;
typedef std::map<int, tr_socket*>::iterator SocketMapIt;
typedef std::map<int, tr_socket*>::const_iterator SocketMapCIt;

	}
}

#endif
