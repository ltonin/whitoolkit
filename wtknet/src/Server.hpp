#ifndef SERVER_HPP 
#define SERVER_HPP 

#include <string>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <cnbicore/CcLog.hpp>
#include <transport/tr_net.h>
#include <transport/tr_tcp.h>
#include <transport/tr_udp.h>

#include <wtkcore/Thread.hpp>
#include <wtkcore/Streamer.hpp>
#include <wtkcore/Time.hpp>

#include "Endpoint.hpp"
#include "Socket.hpp"

namespace wtk {
	namespace net {

class Server : public Socket, protected wtk::core::Thread {
	public:
		Server(size_t bsize = CORE_1MB);
		virtual ~Server(void);
		bool Bind(Ip ip, Port port, int protocol = Socket::TCP);
		bool Bind(Address address, int protocol = Socket::TCP);
		bool Release(void);
		bool IsConnected(void);

		ssize_t Send(const char* message);
		ssize_t Send(const std::string& message);
		ssize_t Send(const void* message, size_t size);
		
		ssize_t Send(const char* message, Address addr);
		ssize_t Send(const std::string& message, Address addr);
		ssize_t Send(const void* message, size_t size, Address addr);
		
		ssize_t SendNot(const char* message, Address addr);
		ssize_t SendNot(const std::string& message, Address addr);
		ssize_t SendNot(const void* message, size_t size, Address addr);
	private:
		void Main(void);
		bool Open(int protocol);
		bool Listen(void);
		int Accept(void);
		void Drop(int fid);

	public:
		wtk::core::Callback1<SocketProxy, Socket*> iOnRelease;
		wtk::core::Callback1<SocketProxy, Socket*> iOnAccept;
		wtk::core::Callback1<SocketProxy, Socket*> iOnDrop;
		wtk::core::Callback2<SocketProxy, Socket*, Address> iOnAcceptPeer;
		wtk::core::Callback2<SocketProxy, Socket*, Address> iOnDropPeer;
		wtk::core::Callback3<SocketProxy, Socket*, Address, wtk::core::Streamer*> iOnRecvPeer;
	protected:
		wtk::core::Callback1<SocketProxy, Socket*> iOnBind;
		wtk::core::Callback1<SocketProxy, Socket*> iOnListen;
		wtk::core::Callback2<SocketProxy, Socket*, Address> iOnSendPeer;
};
	}
}

#endif
