#ifndef WTK_BCI_CPP
#define WTK_BCI_CPP

// OBSOLATED
/*
#include "Bci.hpp"

namespace wtk {
	namespace bci {

Bci::Bci(void) {}
Bci::~Bci(void) {}

void Bci::SetInterface(wtk::net::cnbi::ClTobiId* id, std::string mdesc, unsigned int mtype) {
	this->id_       = id;
	this->idmsg_    = new IDMessage;
	this->idserial_ = new IDSerializerRapid(this->idmsg_);
	this->idmsg_->SetDescription(mdesc);
	this->idmsg_->SetFamilyType(mtype);
}

void Bci::SetInterface(wtk::net::cnbi::ClTobiIc* ic, std::string cname, std::string cdesc) {
	this->ic_       = ic;
	this->icmsg_ = new ICMessage;
	this->icserial_ = new ICSerializerRapid(this->icmsg_);
	this->iccname_  = cname;
	this->iccdesc_  = cdesc;
}

void Bci::CreateMessage(wtk::protocol::TaskSet* taskset) {
	this->icmsg_ = new ICMessage;
	this->icserial_ = new ICSerializerRapid(this->icmsg_);
	this->icc_   = new ICClassifier(this->iccname_, this->iccdesc_, ICClassifier::ValueProb, ICClassifier::LabelBiosig);
	for(auto it = taskset->Begin(); it != taskset->End(); it++) {
		ICClass* cclass = new ICClass(it->first, 0.0f);
		this->icc_->classes.Add(cclass);
	}
	
	this->icmsg_->classifiers.Add(this->icc_);
}

ICMessage* Bci::GetMessage(bool blocking) {
	if (this->ic_->IsAttached() == true) {
		if(blocking == true)
			this->ic_->WaitMessage(this->icserial_);
		else 
			this->ic_->GetMessage(this->icserial_);

		return this->icserial_->message;
	}

	return NULL;
}

IDMessage* Bci::GetEvent(bool blocking) {
}

void Bci::SetValue(std::string label, float value) {
	this->icmsg_->SetValue(this->iccname_, label, value);
}

void Bci::SendMessage(unsigned int idx) {
	if(this->ic_->IsAttached() == true)
		this->ic_->SetMessage(this->icserial_, idx);
}

void Bci::SendEvent(unsigned int value) {
	if(this->id_->IsAttached() == true) {
		this->idmsg_->SetEvent(value);
		this->id_->SetMessage(this->idserial_);
	}
}


bool Bci::ValidateMessage(ICMessage* message, wtk::protocol::TaskSet* taskset) {
	bool retcode = true;
	ICClassifier* icc;	
	try {
		icc = message->GetClassifier(this->iccname_);
		for(auto it = icc->classes.Begin(); it != icc->classes.End(); it++)
			retcode = retcode & taskset->Has(it->first);

	} catch (TCException& e) {
		throw std::runtime_error("Classifier " + this->iccname_ + " in ICMessage not found in taskset");
	}
	return retcode;
}

void Bci::DumpMessage(void) {
	this->icmsg_->Dump();
}

	}
}

*/

#endif
