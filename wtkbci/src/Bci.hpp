#ifndef WTK_BCI_HPP
#define WTK_BCI_HPP

// OBSOLATED

/*
#include <string>

#include <tobiic/ICMessage.hpp>
#include <tobiic/ICSerializerRapid.hpp>
#include <tobiid/IDMessage.hpp>
#include <tobiid/IDSerializerRapid.hpp>
#include <tobicore/TCException.hpp>
#include <tobicore/TCLanguage.hpp>

#include <cnbiloop/ClLoop.hpp>
#include <cnbicore/CcLog.hpp>

#include <wtknet/ClTobiId.hpp>
#include <wtknet/ClTobiIc.hpp>
#include <wtkprotocol/TaskSet.hpp>

namespace wtk {
	namespace bci {

class Bci {
	public:
		Bci(void);
		~Bci(void);


		virtual void SetInterface(wtk::net::cnbi::ClTobiId* id, std::string mdesc, unsigned int mtype = IDMessage::FamilyBiosig);
		virtual void SetInterface(wtk::net::cnbi::ClTobiIc* ic, std::string cname, std::string cdesc = "");

		virtual void CreateMessage(wtk::protocol::TaskSet* taskset);
		virtual ICMessage* GetMessage(bool blocking = true);
		virtual IDMessage* GetEvent(bool blocking = true);

		virtual void SetValue(std::string label, float value);
		virtual void SendMessage(unsigned int idx);
		virtual void SendEvent(unsigned int value);

		virtual bool ValidateMessage(ICMessage* message, wtk::protocol::TaskSet* taskset);

		virtual void DumpMessage(void);
		
		
	protected:
		wtk::net::cnbi::ClTobiId* 	id_ 	  = NULL;
		wtk::net::cnbi::ClTobiIc*	ic_ 	  = NULL;
	
		IDMessage*			idmsg_ 	  = NULL;
		IDSerializerRapid* 		idserial_ = NULL;

		ICMessage*			icmsg_ 	  = NULL;
		ICSerializerRapid* 		icserial_ = NULL;

		ICClassifier*			icc_ = NULL;
		std::string			iccname_;
		std::string 			iccdesc_;


};

	}
}

*/

#endif
