#ifndef WTK_TICSENDER_CPP
#define WTK_TICSENDER_CPP

#include <stdlib.h> 
#include <cmath>

#include <cnbiloop/ClLoop.hpp>
#include <tobiic/ICClass.hpp>
#include <tobiic/ICClassifier.hpp>
#include <tobicore/TCException.hpp>

#include <wtkcore/Core.hpp>
#include <wtkcore/Time.hpp>
#include <wtknet/ClTobiIc.hpp>
#include <wtknet/ClTobiId.hpp>

#define TIME_STEP 63
#define SIN_PERIOD 8000

#define WTK_DEFAULT_CNAME 	"smr"
#define WTK_DEFAULT_PIPE	"/ctrl0"

void usage(void) {
	printf("\nUsage: wtk_ticsender [OPTION]...\n");
	printf("\n  CLASS_LABEL_0		Label for class 0 	[Default: 769]");
	printf("\n  CLASS_LABEL_1		Label for class 1 	[Default: 770]");
	printf("\n  -n			iC pipe name 		[Default: '/ctrl0']");
	printf("\n  -c			Classifier name 	[Default: 'smr']");
	printf("\n  -v			Sine velocity 		[Default: 1.0f (8s period)]");
	printf("\n\n");
}

int main (int argc, char** argv) {

	std::string cname = "smr";
	std::string pipe  = "/ctrl0";
	float velocity = 1.0f;
	unsigned int clabel[2] = {769, 770};
	
	int c;
	int index;
	int nindex = 0;
	int nargs;
	while ((c = getopt(argc, argv, "n:c:v:")) != -1) {
		switch(c) {
			case 'n':
				pipe.assign(optarg);
				break;
			case 'c':
				cname.assign(optarg);
				break;
			case 'v':
				velocity = std::stof(optarg);
				break;
			default:
				usage();
				exit(0);
				break;
		}
	}

	nargs = argc - optind;

	if( nargs > 0 & nargs != 2) {
		printf("[wtk_ticsender] - Only two class labels are allowed: %d provided!\n", nargs);
		usage();
		exit(0);
	}

	for (index = optind; index < argc; index++) {
		clabel[nindex] = std::stoul(argv[index]);
		nindex++;
	}

	
	for(auto i = 0; i<2; i++) {
		printf("[wtk_ticsender] - Class Label %d: %d\n", i, clabel[i]);
	}

	wtk::net::cnbi::ClTobiIc ic(wtk::net::cnbi::ClTobiIc::SetOnly);	
	ICMessage icm;                               
	ICSerializerRapid ics(&icm);
	

	ICClass class_0(std::to_string(clabel[0]), 0.0f);
	ICClass class_1(std::to_string(clabel[1]), 0.0f);

	ICClassifier classifier(cname, "Test classifier", ICClassifier::ValueProb, ICClassifier::LabelBiosig);

	try {
		classifier.classes.Add(&class_0);
		classifier.classes.Add(&class_1);
	} catch (TCException e) { 
                 printf("%s\n", e.GetInfo().c_str());
        }

	icm.classifiers.Add(&classifier);
	icm.Dump();

	ClLoop::Configure();
	if(ClLoop::Connect() == false) {
		printf("[wtk_ticsender] - Cannot connect to loop");
		wtk::core::Core::Exit(0);
	}


	unsigned int i = 0;
	unsigned int t = 0;
	float value;
	while(true) {
		if(ic.IsAttached() == false) {
			printf("[wtk_ticsender] - Try to attach iC at '%s'\n", pipe.c_str());
			ic.TryAttach(pipe, true);
			printf("[wtk_ticsender] - iC attached to '%s', sending streaming\n", pipe.c_str());
		}

		value = sin(2.0f *M_PI * (float)t * (velocity/float(SIN_PERIOD))); 
		t += TIME_STEP;

		icm.SetValue(cname, std::to_string(clabel[0]), std::abs(value));
		icm.SetValue(cname, std::to_string(clabel[1]), 1- std::abs(value));

		ic.SetMessage(&ics, i++); 

		wtk::core::Time::Sleep(TIME_STEP);
	}




	return 0;
}



#endif
