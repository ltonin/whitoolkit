#ifndef WTK_TICSENDER_CPP
#define WTK_TICSENDER_CPP

#include <stdlib.h> 
#include <cmath>

#include <cnbiloop/ClLoop.hpp>
#include <tobiic/ICClass.hpp>
#include <tobiic/ICClassifier.hpp>
#include <tobicore/TCException.hpp>

#include <wtkcore/Core.hpp>
#include <wtkcore/Time.hpp>
#include <wtkprotocol/TaskSet.hpp>

#include <wtknet/IFaceC.hpp>

#define TIME_STEP 63
#define SIN_PERIOD 8000

#define WTK_DEFAULT_CNAME 	"smr"
#define WTK_DEFAULT_PIPE	"/ctrl0"

using namespace wtk::core;
using namespace wtk::protocol;
using namespace wtk::net;
using namespace std;

void usage(void) {
	printf("\nUsage: wtk_ticsender [OPTION]...\n");
	printf("\n  CLASS_LABEL_0		Label for class 0 	[Default: 769]");
	printf("\n  CLASS_LABEL_1		Label for class 1 	[Default: 770]");
	printf("\n  -n			iC pipe name 		[Default: '/ctrl0']");
	printf("\n  -c			Classifier name 	[Default: 'smr']");
	printf("\n  -v			Sine velocity 		[Default: 1.0f (8s period)]");
	printf("\n\n");
}

int main (int argc, char** argv) {

	std::string cname = "smr";
	std::string pipe  = "/ctrl0";
	float velocity = 1.0f;
	unsigned int clabel[2] = {769, 770};
	
	int c;
	int index;
	int nindex = 0;
	int nargs;
	while ((c = getopt(argc, argv, "n:c:v:")) != -1) {
		switch(c) {
			case 'n':
				pipe.assign(optarg);
				break;
			case 'c':
				cname.assign(optarg);
				break;
			case 'v':
				velocity = std::stof(optarg);
				break;
			default:
				usage();
				exit(0);
				break;
		}
	}
	CcCore::OpenLogger("wtk_ticsender");
	ClLoop::Configure();
	if(ClLoop::Connect() == false) {
		CcLogFatal("Cannot connect to loop");
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	nargs = argc - optind;

	if( nargs > 0 & nargs != 2) {
		CcLogFatalS("Only two class labels are allowed: " + to_string(nargs) + " provided!");
		usage();
		CcCore::CloseLogger();
		Core::Exit(0);	
	}

	for (index = optind; index < argc; index++) {
		clabel[nindex] = std::stoul(argv[index]);
		nindex++;
	}


	for(auto i = 0; i<2; i++) {
		CcLogInfoS("Class Label " + to_string(i) + ": " + to_string(clabel[i]));
	}

	IFaceC ic(IFaceC::AsSender);
	TaskSet taskset;

	// Configure TaskSet
	Task task0(0, to_string(clabel[0]));
	Task task1(1, to_string(clabel[1]));
	taskset.Add(task0);	
	taskset.Add(task1);	
	
	// Configure Interface C
	ic.Set(cname, "Simulated classifier");
	try {
		ic.CreateMessage(&taskset);
	} catch (std::runtime_error& e) {
		CcLogFatalS(e.what());
	}

	ic.Dump();
	
	unsigned int i = 0;
	unsigned int t = 0;
	float value;
	while(true) {
		ic.TryAttach(pipe, true);

		value = sin(2.0f *M_PI * (float)t * (velocity/float(SIN_PERIOD))); 
		t += TIME_STEP;

		ic.SetValue(std::to_string(clabel[0]), std::abs(value));
		ic.SetValue(std::to_string(clabel[1]), 1- std::abs(value));
		ic.Send(i++); 

		wtk::core::Time::Sleep(TIME_STEP);
	}

	CcCore::CloseLogger();
	Core::Exit(0);
}



#endif
