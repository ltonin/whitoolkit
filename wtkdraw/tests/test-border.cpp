#include <drawtk.h>

#include <wtkcore/Time.hpp>
#include "../src/Bar.hpp"
#include "../src/Arrow.hpp"
#include "../src/Cross.hpp"
#include "../src/Circle.hpp"

#define ITI_SLEEP 1000

using namespace wtk::core;
using namespace wtk::draw;

int main(int argc, char** argv) {

	float width  = 0.1f;
	float height = 0.3f;
	float thick  = 0.005f;

	dtk_hwnd wnd;

	wnd = dtk_create_window(1024, 768, 0, 0, 16, "hello");
	dtk_make_current_window(wnd);

	Bar bar(height, width, thick);
	Arrow arrow(0.3, 1, 0.02);
	Cross cross(0.5);
	Circle circle(0.5f, 0.01, 1000);


	// ************************************ //
	printf("[test-borders] - Drawing bar\n");
	bar.SetColor(dtk_red, Shape::Stroke);

	dtk_clear_screen(wnd);
	bar.Draw();
	dtk_update_screen(wnd);

	Time::Sleep(ITI_SLEEP);
 
	// ************************************ //
	printf("[test-borders] - Drawing arrow\n");
	
	
	//arrow.Move(-0.5f, 0.2f);
	arrow.SetColor(dtk_plum_dark, Shape::Stroke);
	arrow.SetColor(dtk_plum_light, Shape::Fill);
	
	dtk_clear_screen(wnd);
	arrow.Draw();
	dtk_update_screen(wnd);
	
	
	Time::Sleep(ITI_SLEEP);

	// ************************************ //
	printf("[test-borders] - Drawing cross\n");
	
	cross.SetColor(dtk_red, Shape::Stroke);
	
	dtk_clear_screen(wnd);
	cross.Draw();
	dtk_update_screen(wnd);
	
	
	Time::Sleep(ITI_SLEEP);
	
	// ************************************ //
	printf("[test-borders] - Drawing circle\n");
	
	circle.SetColor(dtk_blue, Shape::Stroke);
	
	dtk_clear_screen(wnd);
	circle.Draw();
	dtk_update_screen(wnd);
	
	
	Time::Sleep(ITI_SLEEP);

	
	dtk_close(wnd);

	return 0;
}

