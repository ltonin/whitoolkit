#include <drawtk.h>
#include <stdlib.h> 
#include <random>

#include <wtkcore/Time.hpp>
#include "../src/Bar.hpp"
#include "../src/Arrow.hpp"
#include "../src/Cross.hpp"
#include "../src/Circle.hpp"
#include "../src/DrawEngine.hpp"

#define ITI_SLEEP 1000

using namespace wtk::core;
using namespace wtk::draw;

void process_generic_event(EventEngine* evt) {
	switch(evt->type) {
		case DTK_EVT_QUIT:
			printf("Event quit\n");
			break;
		case DTK_EVT_KEYBOARD:
			printf("Event keyboard: %c\n", evt->key.sym);
			break;
		case DTK_EVT_MOUSEBUTTON:
			printf("Event mouse - button %d - state %d\n", evt->mouse.button, evt->mouse.state);
			break;
		case DTK_EVT_MOUSEMOTION:
			printf("Event mouse - Position (%d, %d)\n", evt->mouse.x, evt->mouse.y);
			break;
		default:
			break;
	}
};


int main(int argc, char** argv) {

	float width  = 0.1f;
	float height = 0.3f;
	float thick  = 0.005f;
  	
	std::random_device rd;
    	std::mt19937 gen(rd());
    	std::uniform_real_distribution<> dis(0, 2);
	
	DrawEngine Engine(1024, 768, 0, 0, 16, "test-engine");
	EventEngine event;

	Bar bar(0.3f, 0.1f, 0.005);
	Arrow arrow(0.3, 1, 0.02);
	Cross cross(0.5);
	Circle circle(0.3);

	bar.SetColor(dtk_red);
	arrow.SetColor(dtk_blue);
	cross.SetColor(dtk_green);
	circle.SetColor(dtk_yellow);


	bar.Move(-0.5f, 0.5f);
	arrow.Move(0.5f, -0.5f);
	cross.Move(0.5f, 0.5f);
	circle.Move(-0.5f, -0.5f);

	Engine.AddShape(&bar, "bar");
	Engine.AddShape(&arrow, "arrow");
	Engine.AddShape(&cross, "cross");
	Engine.AddShape(&circle, "circle");

	Engine.Open();

	Time::Sleep(1000);
	
	for (auto i=0.0; i<50000; i+=100) {
		
		bar.Move(dis(gen)-1, dis(gen)-1);
		cross.Move(dis(gen)-1, dis(gen)-1);
		arrow.Move(dis(gen)-1, dis(gen)-1);
		circle.Move(dis(gen)-1, dis(gen)-1);

		Time::Sleep(100);
	
		if(Engine.GetEvent(&event, false)) {
			process_generic_event(&event);
		}
		
		if(Engine.EventMousePress()) {
			printf("User pressed mouse\n");
		}
		
		if(Engine.EventKeyboard('q')) {
			printf("User pressed 'q' and asked to quit\n");
			break;
		}
		
		if(Engine.EventQuit()) {
			printf("User pressed window button and asked to quit\n");
			break;
		}
		

	
			

	}

	return 0;
}

