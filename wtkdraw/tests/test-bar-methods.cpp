#include <drawtk.h>

#include <wtkcore/Time.hpp>
#include "../src/Bar.hpp"

#define ITI_SLEEP 1000

using namespace wtk::core;
using namespace wtk::draw;

int main(int argc, char** argv) {

	float width  = 0.3f;
	float height = 0.6f;
	float thick  = 0.01f;

	dtk_hwnd wnd;

	wnd = dtk_create_window(1024, 768, 0, 0, 16, "hello");
	dtk_make_current_window(wnd);

	Bar bar(height, width, thick);



	// ************************************ //
	printf("[test-bar] - Drawing bar\n");
	
	dtk_clear_screen(wnd);
	bar.SetValue(0.5f);
	bar.Draw();
	dtk_update_screen(wnd);
	
	Time::Sleep(ITI_SLEEP);
	
	// ************************************ //
	printf("[test-bar] - Changing border color to blue...");
	
	dtk_clear_screen(wnd);
	bar.SetColor(dtk_blue, Shape::Part::Stroke);
	bar.Draw();
	printf("Done!\n");
	
	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);


	// ************************************ //
	printf("[test-bar] - Changing fill color to cyan...");
	
	dtk_clear_screen(wnd);
	bar.SetColor(dtk_cyan, Shape::Part::Fill);
	bar.Draw();
	printf("Done!\n");
	
	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);
	
	// *********************************** //
	printf("[test-bar] - Hiding bar...");

	dtk_clear_screen(wnd);
	bar.SetAlpha(0.0f);
	bar.Draw();
	printf("Done!\n");

	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);

	// ************************************ //
	printf("[test-bar] - Showing bar...");
	
	dtk_clear_screen(wnd);
	bar.SetAlpha(1.0f);
	bar.Draw();
	printf("Done!\n");
	
	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);

	// ************************************ //
	printf("[test-bar] - Changing transparency of fill...");
	
	dtk_clear_screen(wnd);
	bar.SetAlpha(0.6f, Shape::Part::Fill);
	bar.Draw();
	printf("Done!\n");
	
	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);
	
	
	// ************************************ //
	printf("[test-bar] - Changing color of shape to red...");
	dtk_clear_screen(wnd);
	
	bar.SetColor(dtk_red);
	bar.Draw();
	printf("Done!\n");
	
	dtk_update_screen(wnd);
	Time::Sleep(ITI_SLEEP);

	// ************************************ //
	printf("[test-bar] - Moving to (0.5, 0.5)...\n");
	
	dtk_clear_screen(wnd);
	bar.Move(0.5f, 0.5f);
	bar.Draw();
	dtk_update_screen(wnd);

	Time::Sleep(ITI_SLEEP);
	
	// ************************************ //
	printf("[test-bar] - Rotating -45 degree...\n");
	
	dtk_clear_screen(wnd);
	bar.Rotate(45.0f);
	bar.Draw();
	dtk_update_screen(wnd);

	Time::Sleep(ITI_SLEEP);

	// ************************************ //
	printf("[test-bar] - Rotating 45 degree...\n");
	
	dtk_clear_screen(wnd);
	bar.Rotate(0.0f);
	bar.Draw();
	dtk_update_screen(wnd);

	Time::Sleep(ITI_SLEEP);
	
	// ************************************ //
	printf("[test-bar] - Moving to (-0.5, 0.0)...\n");
	
	dtk_clear_screen(wnd);
	bar.Move(-0.5f, 0.0f);
	bar.Draw();
	dtk_update_screen(wnd);

	Time::Sleep(ITI_SLEEP);
	
	// ************************************ //
	printf("[test-bar] - Set values...");

	float currval = 0.0;
	for(auto i = 0; i < 11; i++) {
		dtk_clear_screen(wnd);
		bar.SetValue(currval);
		bar.Draw();
		dtk_update_screen(wnd);


		currval += 0.1;
		Time::Sleep(100.0f);
	}
	printf("Done!\n");
	
	Time::Sleep(ITI_SLEEP);


	
	dtk_close(wnd);

	return 0;
}

