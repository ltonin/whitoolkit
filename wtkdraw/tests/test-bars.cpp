#include <drawtk.h>

#include <wtkcore/Time.hpp>
#include "../src/Bar.hpp"

#define ITI_SLEEP 1000

using namespace wtk::core;
using namespace wtk::draw;

float sinwave(float t){
	return sin(2.0 * M_PI * t);
}

int main(int argc, char** argv) {

	float width  = 0.3f;
	float height = 0.6f;
	float thick  = 0.01f;

	dtk_hwnd wnd;

	wnd = dtk_create_window(1024, 768, 0, 0, 16, "hello");
	dtk_make_current_window(wnd);

	
	Bar barL(height, width, thick);
	Bar barR(height, width, thick);


	// Setup bars properties
	barL.SetColor(dtk_plum_dark, Shape::Stroke);
	barL.SetColor(dtk_plum_light, Shape::Fill);
	barL.SetAlpha(0.5f, Shape::Fill);
	barL.SetValue(0.0f);
	barL.Move(-0.5f, 0.0f);


	barR.SetColor(dtk_skyblue_dark, Shape::Stroke);
	barR.SetColor(dtk_skyblue_light, Shape::Fill);
	barR.SetAlpha(0.5f, Shape::Fill);
	barR.SetValue(0.0f);
	barR.Move(+0.5f - width, 0.0f);
	
	Time::Sleep(ITI_SLEEP);
	
	float y, t = 0;
	for(auto i=0; i < 101; i++) {
		y = sinwave(t);	
		y > 0 ? barL.SetValue(y) : barR.SetValue(-y);
		t+=0.01;
		
		dtk_clear_screen(wnd);
		barL.Draw();
		barR.Draw();
		dtk_update_screen(wnd);

		Time::Sleep(100.0f);
	}
		
		
	Time::Sleep(ITI_SLEEP);
	dtk_close(wnd);

	return 0;
}

