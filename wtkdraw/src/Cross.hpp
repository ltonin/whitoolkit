#ifndef CROSS_HPP
#define CROSS_HPP


#include "Shape.hpp"
#include <SDL/SDL_opengl.h>

namespace wtk {
	namespace draw {


class Cross : public Shape {
	public:
		Cross(float size, float bold = 0.05f, float thick = 0.01f);
		virtual ~Cross(void);
	
	protected:
		void Create(float width, float height);
		void CreateStroke(float thick);

	private:
		float width_;
		float height_;
		float thick_;
		float bold_;


};

	}
}




#endif
