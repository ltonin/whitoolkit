#ifndef DRAWENGINE_HPP
#define DRAWENGINE_HPP

#include <unordered_map>
#include <string>

#include <drawtk.h>
#include <dtk_event.h>

#include "Shape.hpp"
#include <wtkcore/Thread.hpp>
#include <wtkcore/Time.hpp>

namespace wtk {
	namespace draw {

struct EventEngine {
	int type = -1;
	
	dtk_keyevent   key;
	dtk_mouseevent mouse;
};



class DrawEngine : protected wtk::core::Thread {
	
	public:
		DrawEngine(unsigned int width, unsigned int height, unsigned int x, unsigned int y, unsigned int bpp, std::string title);
		virtual ~DrawEngine(void);

		bool Open(void);
		void Destroy(void);

		bool AddShape(Shape* shape, std::string key);
		bool RemoveShape(std::string key);

		bool GetEvent(EventEngine* event, bool reset = true);
		bool EventKeyboard(char key, bool reset = true);
		bool EventQuit(bool reset = true);
		bool EventMousePress(bool reset = true);
	
	protected:
		virtual void Main(void);
		bool RenderShapes(void);

	private:
		static int engine_event_handler(dtk_hwnd wnd, int type, const union dtk_event* evt);

	private:
		std::unordered_map<std::string, Shape*> shplist_;

		dtk_hwnd     w_wnd_;
		unsigned int w_width_;
		unsigned int w_height_; 
		unsigned int w_x_;
		unsigned int w_y_;
		unsigned int w_bpp_;
		std::string  w_title_;

		static EventEngine event_;


};

EventEngine DrawEngine::event_;

	}
}

#endif
