#ifndef BAR_HPP
#define BAR_HPP

#include "Shape.hpp"
#include <SDL/SDL_opengl.h>

namespace wtk {
	namespace draw {


class Bar : public Shape {
	public:
		Bar(float width, float height, float thick = 0.01f);
		virtual ~Bar(void);

		bool SetValue(float value);

	protected:
		void Create(float width, float height);
		void CreateStroke(float thick);

	private:
		float width_;
		float height_;
		float thick_;


};

	}
}

#endif
