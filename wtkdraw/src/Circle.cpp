#ifndef CIRCLE_CPP
#define CIRCLE_CPP

#include "Circle.hpp"

namespace wtk {
	namespace draw {

Circle::Circle(float radius, float thick, unsigned int npoints) {
	
	this->radius_ = radius;
	this->thick_  = thick;
	this->npoints_ = npoints;

	this->Create(this->radius_, this->radius_);
	this->CreateStroke(this->thick_);
	
}

Circle::~Circle(void) {
	this->Destroy();
}

	
void Circle::Create(float width, float height) {
	
	float r = this->radius_;
	
	if(this->fill_.hshp == NULL) {
		this->init_shape_structure(&this->fill_, this->npoints_+1, this->npoints_+2);
	}


	this->fill_.vertpos[0] = 0.0f;
	this->fill_.vertpos[1] = 0.0f;

	for(auto i=2; i<this->fill_.nvert*2; i+=2) {
		this->fill_.vertpos[i  ] = r*cos((float)i*M_PI*2.0f/(float)this->npoints_);
		this->fill_.vertpos[i+1] = r*sin((float)i*M_PI*2.0f/(float)this->npoints_);
	}
	
	for (unsigned int i = 0; i <this->fill_.nvert*4; i+=4) {
		this->fill_.vertcol[i  ] = this->fill_.color[0];
		this->fill_.vertcol[i+1] = this->fill_.color[1];
		this->fill_.vertcol[i+2] = this->fill_.color[2];
		this->fill_.vertcol[i+3] = this->fill_.color[3];
	}

	for(auto i=0; i<this->fill_.nind; i++)
		this->fill_.ind[i] = i;

	this->fill_.hshp = dtk_create_complex_shape(this->fill_.hshp, 
				       	      this->fill_.nvert, this->fill_.vertpos, this->fill_.vertcol, NULL, 
				       	      this->fill_.nind,  this->fill_.ind, 
				       	      DTK_TRIANGLE_FAN, NULL);

	this->set_shape_properties(&this->fill_);

}

void Circle::CreateStroke(float thick) {
	
	sshape ishp;
	unsigned int stk_nvert, stk_nind;
	float r = (this->radius_) - thick;
	
	// Create inner shape	
	this->copy_shape_structure(&ishp, &this->fill_);

	this->fill_.vertpos[0] = 0.0f;
	this->fill_.vertpos[1] = 0.0f;

	for(auto i=2; i<this->fill_.nvert*2; i+=2) {
		this->fill_.vertpos[i  ] = r*cos((float)i*M_PI*2.0f/(float)this->npoints_);
		this->fill_.vertpos[i+1] = r*sin((float)i*M_PI*2.0f/(float)this->npoints_);
	}

	// Initialize stroke structures
	stk_nvert = this->fill_.nvert*2;
	stk_nind  = this->fill_.nind*2 + 2;

	this->init_shape_structure(&this->stroke_, stk_nvert, stk_nind);
	
	this->create_stroke_structure(&this->stroke_, &this->fill_, &ishp);

	// Create complex shape
	this->stroke_.hshp = dtk_create_complex_shape(this->stroke_.hshp, 
				       this->stroke_.nvert, this->stroke_.vertpos, this->stroke_.vertcol, NULL, 
				       this->stroke_.nind,  this->stroke_.ind, 
				       DTK_TRIANGLE_STRIP, NULL);
	
	this->set_shape_properties(&this->stroke_);
}



	}
}

#endif

