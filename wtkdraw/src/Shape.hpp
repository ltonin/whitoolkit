#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <drawtk.h>
#include <dtk_colors.h>
#include <wtkcore/Semaphore.hpp>

#include <cstring>
#include <stdio.h>
#include <math.h>
#include <algorithm>

namespace wtk {
	namespace draw {


struct sshape {
	
	dtk_hshape hshp = NULL;

	unsigned int nvert;
	unsigned int nind;

	float* vertpos = NULL;
	float* vertcol = NULL;
	unsigned int* ind = NULL;

	float x;
	float y;
	
	float width;
	float height;

	float cx;
	float cy;

	float* color = NULL;
};

class Shape {
	public:
		Shape(void);
		virtual ~Shape(void);
		
		void Draw(void);
		bool Move(float x, float y);
		bool Rotate(float degree);

		void SetAlpha(float alpha, unsigned int spart = -1); 
		void SetColor(const float* color, unsigned int spart = -1); 
		
		void Destroy(void);

	protected:
		virtual void Create(float width, float height) = 0;
		virtual void CreateStroke(float thick);
		bool IsSet(sshape* sshp);
	
		
		void init_shape_structure(sshape* sshp, unsigned int nvert, unsigned int nind);
		void copy_shape_structure(sshape* ishp, sshape * oshp);
		void create_stroke_structure(sshape* dest, sshape* outer, sshape* inner);
		void set_shape_properties(sshape* sshp);
		void destroy_shape_structure(sshape* sshp);
		
	private:
		void set_color_shape(sshape* sshp, const float* color, unsigned int mask);
		void set_alpha_shape(sshape* sshp, float alpha);
		
		void set_shape_dimension(sshape* sshp);
		void set_shape_centroid(sshape* sshp);
		void set_shape_position(sshape* sshp) {};

	public:
		enum Part {Fill, Stroke};
	protected:
		
		sshape fill_;
		sshape stroke_;

		wtk::core::Semaphore sem_shape_;

};


	}
}

#endif
