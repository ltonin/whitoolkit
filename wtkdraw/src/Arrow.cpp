#ifndef ARROW_CPP
#define ARROW_CPP

#include "Arrow.hpp"

namespace wtk {
	namespace draw {

Arrow::Arrow(float height, float width, float thick) {
	
	this->width_  = width;
	this->height_ = height;
	this->thick_  = thick;

	this->Create(this->width_, this->height_);
	this->CreateStroke(this->thick_);
	
}

Arrow::~Arrow(void) {
	this->Destroy();
}

	
void Arrow::Create(float width, float height) {

	float w = width;
	float h = height;
	
	if(this->fill_.hshp == NULL)
		this->init_shape_structure(&this->fill_, 7, 7);
	
	this->fill_.vertpos[0]  =  0.0f   - w/2.0f;
	this->fill_.vertpos[1]  =  0.0f;
	this->fill_.vertpos[2]  =  w/3.0f - w/2.0f;
	this->fill_.vertpos[3]  =  h/2.0f;
	this->fill_.vertpos[4]  =  w/3.0f - w/2.0f;
	this->fill_.vertpos[5]  =  h/6.0f;
	this->fill_.vertpos[6]  =  w 	  - w/2.0f;
	this->fill_.vertpos[7]  =  h/6.0f;
	this->fill_.vertpos[8]  =  w 	  - w/2.0f;
	this->fill_.vertpos[9]  = -h/6.0f;
	this->fill_.vertpos[10] =  w/3.0f - w/2.0f;
	this->fill_.vertpos[11] = -h/6.0f;
	this->fill_.vertpos[12] =  w/3.0f - w/2.0f;
	this->fill_.vertpos[13] = -h/2.0f;

	for (unsigned int i = 0; i <this->fill_.nvert*4; i+=4) {
		this->fill_.vertcol[i  ] = this->fill_.color[0];
		this->fill_.vertcol[i+1] = this->fill_.color[1];
		this->fill_.vertcol[i+2] = this->fill_.color[2];
		this->fill_.vertcol[i+3] = this->fill_.color[3];
	}
	
	this->fill_.ind[0] = 1;
	this->fill_.ind[1] = 6;
	this->fill_.ind[2] = 0;
	this->fill_.ind[3] = 2;
	this->fill_.ind[4] = 5;
	this->fill_.ind[5] = 3;
	this->fill_.ind[6] = 4;


	this->fill_.hshp = dtk_create_complex_shape(this->fill_.hshp, 
				       	      this->fill_.nvert, this->fill_.vertpos, this->fill_.vertcol, NULL, 
				       	      this->fill_.nind, this->fill_.ind, 
				       	      DTK_TRIANGLE_STRIP, NULL);

	this->set_shape_properties(&this->fill_);

}

void Arrow::CreateStroke(float thick) {

	sshape ishp;
	unsigned int stk_nvert, stk_nind;
	float w, h;

	w = this->width_;
	h = this->height_;
	
	// Create inner shape	
	this->copy_shape_structure(&ishp, &this->fill_);
	
	ishp.vertpos[0]  +=   thick*(w/(1.5*h));
	ishp.vertpos[1]  +=   0.0f;
	ishp.vertpos[2]  +=  -thick;
	ishp.vertpos[3]  +=  -thick*(std::tan(std::atan2(this->fill_.vertpos[3], this->fill_.vertpos[2])/2.0f) - M_PI/2.0f);
	ishp.vertpos[4]  +=  -thick;
	ishp.vertpos[5]  +=  -thick;
	ishp.vertpos[6]  +=  -thick;
	ishp.vertpos[7]  +=  -thick;
	ishp.vertpos[8]  +=  -thick;
	ishp.vertpos[9]  +=   thick;
	ishp.vertpos[10] +=  -thick;
	ishp.vertpos[11] +=   thick;
	ishp.vertpos[12] +=  -thick;
	ishp.vertpos[13] +=  -thick*(std::tan(std::atan2(this->fill_.vertpos[13], this->fill_.vertpos[12])/2.0f) + M_PI/2.0f);
	
	// Initialize stroke structures
	stk_nvert = this->fill_.nvert*2;
	stk_nind  = this->fill_.nind*2 + 2;

	this->init_shape_structure(&this->stroke_, stk_nvert, stk_nind);
	
	this->create_stroke_structure(&this->stroke_, &this->fill_, &ishp);


	// Create complex shape
	this->stroke_.hshp = dtk_create_complex_shape(this->stroke_.hshp, 
				       this->stroke_.nvert, this->stroke_.vertpos, this->stroke_.vertcol, NULL, 
				       this->stroke_.nind,  this->stroke_.ind, 
				       DTK_TRIANGLE_STRIP, NULL);
	
	this->set_shape_properties(&this->stroke_);

}


	}
}

#endif

