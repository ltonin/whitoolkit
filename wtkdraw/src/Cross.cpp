#ifndef CROSS_CPP
#define CROSS_CPP

#include "Cross.hpp"

namespace wtk {
	namespace draw {

Cross::Cross(float size, float bold, float thick) {
	
	this->width_  = size;
	this->height_ = size;
	this->thick_  = thick;
	this->bold_   = bold;

	this->Create(this->width_, this->height_);
	this->CreateStroke(this->thick_);
	
}

Cross::~Cross(void) {
	this->Destroy();
}

	
void Cross::Create(float width, float height) {
	
	float bold = this->bold_;
	float size = width;

	if(this->fill_.hshp == NULL)
		this->init_shape_structure(&this->fill_, 12, 12);
	
	this->fill_.vertpos[0]  = -size/2.0f;
	this->fill_.vertpos[1]  = -bold/2.0f;

	this->fill_.vertpos[2]  = -size/2.0f;
	this->fill_.vertpos[3]  =  bold/2.0f;

	this->fill_.vertpos[4]  = -bold/2.0f;
	this->fill_.vertpos[5]  =  bold/2.0f;

	this->fill_.vertpos[6]  = -bold/2.0f;
	this->fill_.vertpos[7]  =  size/2.0f;

	this->fill_.vertpos[8]  =  bold/2.0f;
	this->fill_.vertpos[9]  =  size/2.0f;
	
	this->fill_.vertpos[10] =  bold/2.0f;
	this->fill_.vertpos[11] =  bold/2.0f;
	
	this->fill_.vertpos[12] =  size/2.0f;
	this->fill_.vertpos[13] =  bold/2.0f;
	
	this->fill_.vertpos[14] =  size/2.0f;
	this->fill_.vertpos[15] = -bold/2.0f;

	this->fill_.vertpos[16] =  bold/2.0f;
	this->fill_.vertpos[17] = -bold/2.0f;
	
	this->fill_.vertpos[18] =  bold/2.0f;
	this->fill_.vertpos[19] = -size/2.0f;
	
	this->fill_.vertpos[20] = -bold/2.0f;
	this->fill_.vertpos[21] = -size/2.0f;
	
	this->fill_.vertpos[22] = -bold/2.0f;
	this->fill_.vertpos[23] = -bold/2.0f;
	
	for (unsigned int i = 0; i <this->fill_.nvert*4; i+=4) {
		this->fill_.vertcol[i  ] = this->fill_.color[0];
		this->fill_.vertcol[i+1] = this->fill_.color[1];
		this->fill_.vertcol[i+2] = this->fill_.color[2];
		this->fill_.vertcol[i+3] = this->fill_.color[3];
	}
	
	this->fill_.ind[0]  = 0;
	this->fill_.ind[1]  = 1;
	this->fill_.ind[2]  = 11;
	this->fill_.ind[3]  = 2;
	this->fill_.ind[4]  = 4;
	this->fill_.ind[5]  = 3;
	this->fill_.ind[6]  = 9;
	this->fill_.ind[7]  = 10;
	this->fill_.ind[8]  = 5;
	this->fill_.ind[9]  = 8;
	this->fill_.ind[10] = 6;
	this->fill_.ind[11] = 7;


	this->fill_.hshp = dtk_create_complex_shape(this->fill_.hshp, 
				       	      this->fill_.nvert, this->fill_.vertpos, this->fill_.vertcol, NULL, 
				       	      this->fill_.nind, this->fill_.ind, 
				       	      DTK_TRIANGLE_STRIP, NULL);

	this->set_shape_properties(&this->fill_);

}

void Cross::CreateStroke(float thick) {

	sshape ishp;
	unsigned int stk_nvert, stk_nind;
	float w, h;

	w = this->width_;
	h = this->height_;
	
	// Create inner shape	
	this->copy_shape_structure(&ishp, &this->fill_);
	
	ishp.vertpos[0]  +=  thick;
	ishp.vertpos[1]  +=  thick;
	ishp.vertpos[2]  +=  thick;
	ishp.vertpos[3]  += -thick;
	ishp.vertpos[4]  +=  thick;
	ishp.vertpos[5]  += -thick;
	ishp.vertpos[6]  +=  thick;
	ishp.vertpos[7]  += -thick;
	ishp.vertpos[8]  += -thick;
	ishp.vertpos[9]  += -thick;
	ishp.vertpos[10] += -thick;
	ishp.vertpos[11] += -thick;
	ishp.vertpos[12] += -thick;
	ishp.vertpos[13] += -thick;
	ishp.vertpos[14] += -thick;
	ishp.vertpos[15] +=  thick;
	ishp.vertpos[16] += -thick;
	ishp.vertpos[17] +=  thick;
	ishp.vertpos[18] += -thick;
	ishp.vertpos[19] +=  thick;
	ishp.vertpos[20] +=  thick;
	ishp.vertpos[21] +=  thick;
	ishp.vertpos[22] +=  thick;
	ishp.vertpos[23] +=  thick;
	
	// Initialize stroke structures
	stk_nvert = this->fill_.nvert*2;
	stk_nind  = this->fill_.nind*2 + 2;

	this->init_shape_structure(&this->stroke_, stk_nvert, stk_nind);
	
	this->create_stroke_structure(&this->stroke_, &this->fill_, &ishp);


	// Create complex shape
	this->stroke_.hshp = dtk_create_complex_shape(this->stroke_.hshp, 
				       this->stroke_.nvert, this->stroke_.vertpos, this->stroke_.vertcol, NULL, 
				       this->stroke_.nind,  this->stroke_.ind, 
				       DTK_TRIANGLE_STRIP, NULL);
	
	this->set_shape_properties(&this->stroke_);

}



	}
}

#endif

