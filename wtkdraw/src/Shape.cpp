#ifndef SHAPE_CPP
#define SHAPE_CPP

#include "Shape.hpp"

namespace wtk {
	namespace draw {


Shape::Shape(void) {
}

Shape::~Shape(void) {
}

void Shape::Draw(void) {

	this->sem_shape_.Wait();
	if(this->IsSet(&this->fill_) == true) {
		dtk_draw_shape(this->fill_.hshp);
	}
	
	if(this->IsSet(&this->stroke_) == true) {
		dtk_draw_shape(this->stroke_.hshp);
	}
	this->sem_shape_.Post();
}

bool Shape::Move(float x, float y) {
	this->sem_shape_.Wait();
	
	if(this->IsSet(&this->fill_) == true) {
		dtk_move_shape(this->fill_.hshp, x, y);
		this->fill_.x = x;
		this->fill_.y = y;
	}

	if(this->IsSet(&this->stroke_) == true) {
		dtk_move_shape(this->stroke_.hshp, x, y);
		this->stroke_.x = x;
		this->stroke_.y = y;
	}
	this->sem_shape_.Post();
}

bool Shape::Rotate(float degree) {
	this->sem_shape_.Wait();
	if(this->IsSet(&this->fill_))
		dtk_rotate_shape(this->fill_.hshp, degree);
	if(this->IsSet(&this->stroke_))
		dtk_rotate_shape(this->stroke_.hshp, degree);
	this->sem_shape_.Post();
}

void Shape::SetAlpha(float alpha, unsigned int spart) {
	switch(spart) {
		case Part::Fill:
			this->set_alpha_shape(&this->fill_, alpha);
			break;
		case Part::Stroke:
			this->set_alpha_shape(&this->stroke_, alpha);
			break;
		default:
			this->set_alpha_shape(&this->fill_, alpha);
			this->set_alpha_shape(&this->stroke_, alpha);
			break;
	}
}

void Shape::SetColor(const float* color, unsigned int spart) {
	switch(spart) {
		case Part::Fill:
			this->set_color_shape(&this->fill_, color, DTK_IGNA);
			break;
		case Part::Stroke:
			this->set_color_shape(&this->stroke_, color, DTK_IGNA);
			break;
		default:
			this->set_color_shape(&this->fill_, color, DTK_IGNA);
			this->set_color_shape(&this->stroke_, color, DTK_IGNA);
			break;
	}
}

void Shape::Destroy(void) {
	this->destroy_shape_structure(&this->fill_);
	this->destroy_shape_structure(&this->stroke_);
}

bool Shape::IsSet(sshape* sshp) {
	if(sshp->hshp == NULL)
		return false;

	return true;
}

void Shape::set_color_shape(sshape* sshp, const float* color, unsigned int mask) {

	
	this->sem_shape_.Wait();
	if (this->IsSet(sshp)) {
		for (auto i=0; i<4; i++)
			if (!(mask & (1<<i)))
				sshp->color[i] = color[i];

		dtk_setcolor_shape(sshp->hshp, color, mask);
	}
	
	this->sem_shape_.Post();
}


void Shape::set_alpha_shape(sshape* sshp, float alpha) {
	
	float color[4] = {0.0f, 0.0f, 0.0f, alpha};
	this->set_color_shape(sshp, color, DTK_IGNRGB);
}






void Shape::CreateStroke(float thick) {}


void Shape::set_shape_properties(sshape* sshp) {
	
	// Set shape centroid
	this->set_shape_centroid(sshp);
	
	// Set shape dimensions
	this->set_shape_dimension(sshp);
	
	// Set shape position
	this->set_shape_position(sshp);
}

void Shape::set_shape_centroid(sshape* sshp) {

	float a = 0.0f, cx = 0.0f, cy = 0.0f;
	float c_x, c_y, n_x, n_y;

	if (this->IsSet(sshp)) {
		
		for(auto i=0; i<sshp->nvert*2 - 2; i+=2) {
			c_x = sshp->vertpos[i];
			c_y = sshp->vertpos[i+1];
			n_x = sshp->vertpos[i+2];
			n_y = sshp->vertpos[i+3];

			a  += 0.5*(c_x*n_y - n_x*c_y); 
			cx += (c_x + n_x)*(c_x*n_y - n_x*c_y);
			cy += (c_y + n_y)*(c_x*n_y - n_x*c_y);
		}

		c_x = sshp->vertpos[sshp->nvert*2 - 2];
		c_y = sshp->vertpos[sshp->nvert*2 - 1];
		n_x = sshp->vertpos[0];
		n_y = sshp->vertpos[1];

		cx += (c_x + n_x)*(c_x*n_y - n_x*c_y);
		cy += (c_y + n_y)*(c_x*n_y - n_x*c_y);
	
		sshp->cx = std::abs(cx/(6*a));
		sshp->cy = std::abs(cy/(6*a));
	}
}

void Shape::set_shape_dimension(sshape* sshp) {

	float tx, ty;
	float xmax = 0.0f, xmin = 0.0f, ymax = 0.0f, ymin = 0.0f;

	if (this->IsSet(sshp)) {
		for(auto i=0; i<sshp->nvert*2; i+=2) {
			tx = sshp->vertpos[i];
			ty = sshp->vertpos[i+1];
			xmax = std::max(xmax, tx);
			xmin = std::min(xmin, tx);
			ymax = std::max(ymax, ty);
			ymin = std::min(ymin, ty);
		}

		sshp->width  = std::abs(xmax - xmin);
		sshp->height = std::abs(ymax - ymin);
	}
}



void Shape::init_shape_structure(sshape* sshp, unsigned int nvert, unsigned int nind) {

	sshp->hshp    = NULL;
	sshp->nvert   = nvert;
	sshp->nind    = nind;
	sshp->vertpos = new float[2*sshp->nvert];
	sshp->vertcol = new float[4*sshp->nvert];;
	sshp->ind     = new unsigned int[sshp->nind];
	sshp->x       = 0.0f;
	sshp->y       = 0.0f;
	sshp->cx      = 0.0f;
	sshp->cy      = 0.0f;
	sshp->width   = 0.0f;
	sshp->height  = 0.0f;
	sshp->color   = new float[4]; 	
	for(unsigned int i = 0; i<4; i++)
		sshp->color[i] = 1.0f;
}

void Shape::copy_shape_structure(sshape* dest, sshape* orig) {

	this->init_shape_structure(dest, orig->nvert, orig->nind);

	dest->x      = orig->x;
	dest->y      = orig->y;
	dest->cx     = orig->cx;
	dest->cy     = orig->cy;
	dest->width  = orig->width;
	dest->height = orig->height;

	for(auto i=0; i<dest->nvert*2; i++)
		dest->vertpos[i] = orig->vertpos[i];

	for(auto i=0; i<dest->nvert*4; i++)
		dest->vertcol[i] = orig->vertcol[i];
	
	for(auto i=0; i<dest->nind; i++)
		dest->ind[i] = orig->ind[i];

	for(auto i=0; i<4; i++)
		dest->color[i] = orig->color[i];

}

void Shape::create_stroke_structure(sshape* dest, sshape* outer, sshape* inner) {

	unsigned int u;
	
	unsigned int stk_nvert = dest->nvert;
	unsigned int stk_nind  = dest->nind;

	// Populate stroke structure - Vertice positions
	u = 0;
	for(auto i=0; i<stk_nvert*2; i+=4) {
		dest->vertpos[i  ] = outer->vertpos[u];
		dest->vertpos[i+1] = outer->vertpos[u+1];
		dest->vertpos[i+2] = inner->vertpos[u];
		dest->vertpos[i+3] = inner->vertpos[u+1];
		u+=2;
	}
	
	// Populate stroke structure - Vertice colors
	u = 0;
	for (auto i=0; i<stk_nvert*4; i+=8) {
		dest->vertcol[i  ] = outer->vertcol[u];
		dest->vertcol[i+1] = outer->vertcol[u+1];
		dest->vertcol[i+2] = outer->vertcol[u+2];
		dest->vertcol[i+3] = outer->vertcol[u+3];
		dest->vertcol[i+4] = inner->vertcol[u];
		dest->vertcol[i+5] = inner->vertcol[u+1];
		dest->vertcol[i+6] = inner->vertcol[u+2];
		dest->vertcol[i+7] = inner->vertcol[u+3];
		u+=4;
	}
	
	// Populate stroke structure - Indices
	for (auto i=0; i<(stk_nind -2); i++)
		dest->ind[i] = i;
	
	dest->ind[stk_nind-2] = 0;
	dest->ind[stk_nind-1] = 1;
}


void Shape::destroy_shape_structure(sshape* sshp) {
	
	if (this->IsSet(sshp)) {
		dtk_destroy_shape(sshp->hshp);
		delete sshp->vertpos;
		delete sshp->vertcol;
		delete sshp->ind;
		delete sshp->color;
	}
}

	}
}

#endif

