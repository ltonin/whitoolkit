#ifndef ARROW_HPP
#define ARROW_HPP


#include "Shape.hpp"
#include <SDL/SDL_opengl.h>

namespace wtk {
	namespace draw {


class Arrow : public Shape {
	public:
		Arrow(float width, float height, float thick = 0.01f);
		virtual ~Arrow(void);
	
	protected:
		void Create(float width, float height);
		void CreateStroke(float thick);

	private:
		float width_;
		float height_;
		float thick_;


};

	}
}




#endif
