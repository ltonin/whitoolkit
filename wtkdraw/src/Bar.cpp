#ifndef BAR_CPP
#define BAR_CPP

#include "Bar.hpp"

namespace wtk {
	namespace draw {


Bar::Bar(float height, float width, float thick) {
	
	this->width_  = width;
	this->height_ = height;
	this->thick_  = thick;
	
	this->Create(this->width_, this->height_);
	this->CreateStroke(this->thick_);

}

Bar::~Bar(void) {
	this->Destroy();
}

void Bar::Create(float width, float height) {

	float w = width;
	float h = height;

	if(this->fill_.hshp == NULL) {
		this->init_shape_structure(&this->fill_, 4, 4);
	}


	this->fill_.vertpos[0] = -w/2.0f;
	this->fill_.vertpos[1] =  0.0f;
	this->fill_.vertpos[2] = -w/2.0f;
	this->fill_.vertpos[3] =  h;
	this->fill_.vertpos[4] =  w/2.0f;
	this->fill_.vertpos[5] =  h;
	this->fill_.vertpos[6] =  w/2.0f;
	this->fill_.vertpos[7] =  0.0f;

	for (unsigned int i = 0; i <this->fill_.nvert*4; i+=4) {
		this->fill_.vertcol[i  ] = this->fill_.color[0];
		this->fill_.vertcol[i+1] = this->fill_.color[1];
		this->fill_.vertcol[i+2] = this->fill_.color[2];
		this->fill_.vertcol[i+3] = this->fill_.color[3];
	}
	
	this->fill_.ind[0] = 1;
	this->fill_.ind[1] = 0;
	this->fill_.ind[2] = 2;
	this->fill_.ind[3] = 3;

	this->fill_.hshp = dtk_create_complex_shape(this->fill_.hshp, 
				       	      this->fill_.nvert, this->fill_.vertpos, this->fill_.vertcol, NULL, 
				       	      this->fill_.nind,  this->fill_.ind, 
				       	      DTK_TRIANGLE_STRIP, NULL);

	this->set_shape_properties(&this->fill_);
}

void Bar::CreateStroke(float thick) {
	
	sshape ishp;
	unsigned int stk_nvert, stk_nind;
	float w, h;

	w = this->width_;
	h = this->height_;
	
	// Create inner shape	
	this->copy_shape_structure(&ishp, &this->fill_);

	ishp.vertpos[0] = -w/2.0f + thick;
	ishp.vertpos[1] =  0.0f   + thick;
	ishp.vertpos[2] = -w/2.0f + thick;
	ishp.vertpos[3] =  h      - thick;
	ishp.vertpos[4] =  w/2.0f - thick;
	ishp.vertpos[5] =  h      - thick;
	ishp.vertpos[6] =  w/2.0f - thick;
	ishp.vertpos[7] =  0.0f   + thick;

	// Initialize stroke structures
	stk_nvert = this->fill_.nvert*2;
	stk_nind  = this->fill_.nind*2 + 2;

	this->init_shape_structure(&this->stroke_, stk_nvert, stk_nind);
	
	this->create_stroke_structure(&this->stroke_, &this->fill_, &ishp);

	// Create complex shape
	this->stroke_.hshp = dtk_create_complex_shape(this->stroke_.hshp, 
				       this->stroke_.nvert, this->stroke_.vertpos, this->stroke_.vertcol, NULL, 
				       this->stroke_.nind,  this->stroke_.ind, 
				       DTK_TRIANGLE_STRIP, NULL);
	
	this->set_shape_properties(&this->stroke_);



}

bool Bar::SetValue(float value) {
	this->Create(this->width_, this->height_*value);
}


	}
}

#endif
