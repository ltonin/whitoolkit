#ifndef CIRCLE_HPP
#define CIRCLE_HPP


#include "Shape.hpp"
#include <SDL/SDL_opengl.h>

#define CIRCLE_NPOINTS 100

namespace wtk {
	namespace draw {


class Circle : public Shape {
	public:
		Circle(float radius, float thick = 0.01f, unsigned int npoints = 100);
		virtual ~Circle(void);
	
	protected:
		void Create(float width, float height);
		void CreateStroke(float thick);

	private:
		float radius_;
		float thick_;
		unsigned int npoints_;


};

	}
}




#endif
