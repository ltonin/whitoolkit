#ifndef DRAWENGINE_CPP
#define DRAWENGINE_CPP

#include "DrawEngine.hpp"

namespace wtk {
	namespace draw {

DrawEngine::DrawEngine(unsigned int width, unsigned int height, unsigned int x, unsigned int y, unsigned int bpp, std::string title) {
	this->w_wnd_	= NULL;
	this->w_width_  = width;
	this->w_height_ = height; 
	this->w_x_	= x;
	this->w_y_	= y;
	this->w_bpp_	= bpp;
	this->w_title_ 	= title;

}

DrawEngine::~DrawEngine(void) {
}

void DrawEngine::Destroy(void) {
	if(this->IsRunning()) {
		this->Stop();
		this->Join();
	}

	if(this->w_wnd_ != NULL) {
		//dtk_close(this->w_wnd_);
	}
}

bool DrawEngine::Open(void) {

	bool res = false;

	if(this->IsRunning() == false) {
		this->Start();
		res = true;
	}

	return res;
}

bool DrawEngine::AddShape(Shape* shape, std::string key) {
	std::pair<std::string, Shape*> element (key, shape);
	return this->shplist_.insert(element).second;
}

bool DrawEngine::RemoveShape(std::string key) {
	return this->shplist_.erase(key);
}


void DrawEngine::Main(void) {

	if(this->IsRunning() == false)
		return;


	this->w_wnd_ = dtk_create_window(this->w_width_, this->w_height_,
					 this->w_x_, this->w_y_, this->w_bpp_,
					 this->w_title_.c_str());

	if(this->w_wnd_ != NULL) {
		dtk_make_current_window(this->w_wnd_);
		dtk_set_event_handler(this->w_wnd_, this->engine_event_handler);
	}
	
	while(this->IsRunning()) {
		dtk_clear_screen(this->w_wnd_);
		this->RenderShapes();
		dtk_update_screen(this->w_wnd_);

		dtk_process_events(this->w_wnd_);

		wtk::core::Time::Sleep(25.0f);
	}

	dtk_close(this->w_wnd_);

}

bool DrawEngine::RenderShapes(void) {

	for(auto it=this->shplist_.begin(); it != this->shplist_.end(); it++) {
		it->second->Draw();
	}
}

int DrawEngine::engine_event_handler(dtk_hwnd wnd, int type, const union dtk_event* evt) {

	event_.type = type;
	
	switch (type) {
		case DTK_EVT_QUIT:
			break;
		case DTK_EVT_REDRAW:
			break;
		case DTK_EVT_KEYBOARD:
			event_.key = evt->key;
			break;
		case DTK_EVT_MOUSEBUTTON:
			event_.mouse = evt->mouse;
			break;
		case DTK_EVT_MOUSEMOTION:
			event_.mouse = evt->mouse;
			break;
	}

}

bool DrawEngine::EventKeyboard(char key, bool reset) {

	bool ret = false;
	if(DrawEngine::event_.type == DTK_EVT_KEYBOARD & DrawEngine::event_.key.sym == key) {
		ret = true;
		if (reset)
			DrawEngine::event_.type = -1;
	}

	return ret;
}

bool DrawEngine::EventQuit(bool reset) {

	bool ret = false;
	if(DrawEngine::event_.type == DTK_EVT_QUIT) {
		ret = true;
		if (reset)
			DrawEngine::event_.type = -1;
	}

	return ret;
}

bool DrawEngine::EventMousePress(bool reset) {

	bool ret = false;
	if(DrawEngine::event_.type == DTK_EVT_MOUSEBUTTON && DrawEngine::event_.mouse.state == 1) {
		ret = true;
		if (reset)
			DrawEngine::event_.type = -1;
	}

	return ret;
}

bool DrawEngine::GetEvent(EventEngine* event, bool reset) {

	bool ret = false;

	if(DrawEngine::event_.type != -1) {
		ret = true;
		event->type  =  DrawEngine::event_.type;
		event->key   =  DrawEngine::event_.key;
		event->mouse =  DrawEngine::event_.mouse;
		if (reset)
			DrawEngine::event_.type = -1;
	}

	return ret;
}





	}
}

#endif
