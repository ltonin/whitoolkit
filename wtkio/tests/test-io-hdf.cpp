#include <H5Cpp.h>

using namespace H5;

const int DIM0 = 5;
const int DIM1 = 7;
const int RANK = 2;
const H5std_string	FILE_NAME("testH5.h5");
const H5std_string	DATASET_NAME("dset");

int main(int argc, char** argv) {

	// Data initialization.
	int i, j;
	hsize_t datadims[2];
	hsize_t attrdims[2];
	datadims[0] = DIM0;
	datadims[1] = DIM1;
	attrdims[1] = 2;
	int attrdata[2] = {15, 30};

	H5File file(FILE_NAME, H5F_ACC_TRUNC);	
	
	DataSpace dataspace(RANK, datadims);
	DataSpace attrspace(1, attrdims);
	DataSet* dataset;
	Attribute* attribute;
	
	dataset = new DataSet(file.createDataSet("dset1", PredType::STD_I32BE, dataspace));
	attribute = new Attribute(dataset->createAttribute("psd-attribute", PredType::STD_I32BE, attrspace));
	
	int data[DIM0][DIM1];	    // buffer for data to write
	
	for (j = 0; j < DIM0; j++)
	    for (i = 0; i < DIM1; i++)
	     data[j][i] = i * 6 + j + 1;
	
	dataset->write(data, PredType::NATIVE_INT);
	attribute->write(PredType::NATIVE_INT, attrdata);

	dataset = new DataSet(file.createDataSet("dset2", PredType::STD_I32BE, dataspace));
	
	for (j = 0; j < DIM0; j++)
	    for (i = 0; i < DIM1; i++)
	     data[j][i] = i + j;
	
	dataset->write(data, PredType::NATIVE_INT);

	Group* group;
	group = new Group(file.createGroup("/group1"));

	for (j = 0; j < DIM0; j++)
	    for (i = 0; i < DIM1; i++)
	     data[j][i] = i*10 + j*5;

	dataset = new DataSet(group->createDataSet("dsetgroup", PredType::STD_I32BE, dataspace));
	dataset->write(data, PredType::NATIVE_INT);

	//
	//
	//// Open an existing file and dataset.
	//H5File file(FILE_NAME, H5F_ACC_RDWR);
	//DataSet dataset = file.openDataSet(DATASET_NAME);

	//// Write the data to the dataset using default memory space, file
	//// space, and transfer properties.
	//dataset.write(data, PredType::NATIVE_INT);
	return 0;
}
