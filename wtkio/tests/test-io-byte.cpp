#include <fstream>

#include "wtk_io_utilities.hpp"

using namespace std;
using namespace wtk::io;

int main(int argc, char** argv) {

	ifstream ifs;
	ofstream ofs;

	int t_int 	    = -19;
	unsigned int t_uint = 87;
	double t_double     = 25.7839;
	float  t_float      = 74.493;
	char t_char 	    = 'z';



	// Writing
	ofs.open("./test_int.dat", std::ios::binary);
	wtk_write_byte(ofs, t_int);
	printf("[int] - Wrote %d on file\n", t_int);
	t_int = 0;
	ofs.close();

	ofs.open("./test_uint.dat", std::ios::binary);
	wtk_write_byte(ofs, t_uint);
	printf("[uint] - Wrote %u on file\n", t_uint);
	t_uint = 0;
	ofs.close();

	ofs.open("./test_double.dat", std::ios::binary);
	wtk_write_byte(ofs, t_double);
	printf("[double] - Wrote %f on file\n", t_double);
	t_double = 0;
	ofs.close();

	ofs.open("./test_float.dat", std::ios::binary);
	wtk_write_byte(ofs, t_float);
	printf("[float] - Wrote %f on file\n", t_float);
	t_float = 0;
	ofs.close();

	ofs.open("./test_char.dat", std::ios::binary);
	wtk_write_byte(ofs, t_char);
	printf("[char] - Wrote %c on file\n", t_char);
	t_char = '\0';
	ofs.close();
	
	// Reading
	ifs.open("./test_int.dat", std::ios::in | std::ios::binary);
	wtk_read_byte(ifs, t_int);
	printf("[int] - Read %d from file\n", t_int);
	ifs.close();

	ifs.open("./test_uint.dat", std::ios::in | std::ios::binary);
	wtk_read_byte(ifs, t_uint);
	printf("[uint] - Read %u from file\n", t_uint);
	ifs.close();

	ifs.open("./test_double.dat", std::ios::in | std::ios::binary);
	wtk_read_byte(ifs, t_double);
	printf("[double] - Read %f from file\n", t_double);
	ifs.close();
	
	ifs.open("./test_float.dat", std::ios::in | std::ios::binary);
	wtk_read_byte(ifs, t_float);
	printf("[float] - Read %f from file\n", t_float);
	ifs.close();
	
	ifs.open("./test_char.dat", std::ios::in | std::ios::binary);
	wtk_read_byte(ifs, t_char);
	printf("[char] - Read %c from file\n", t_char);
	ifs.close();
	
	return 0;
}
