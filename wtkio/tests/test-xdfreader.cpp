#include <iostream>
#include <Eigen/Dense>

#include "XDFReader.hpp"

using namespace wtk::io;

int main(int argc, char** argv) {

	
	XDFReader xdf;
	int ns = 1;
	int nstot = 0;
	int fsize = 1;

	int neeg = 0;
	int nexg = 0;
	int ntri = 0;
	int nevt = 0;
	int sf = 0;
	int nsamples = 0;

	xdfevent* events;

	xdf.Open(argv[1]);

	xdf.GetInfo(XDF_NEEG, &neeg);
	xdf.GetInfo(XDF_NEXG, &nexg);
	xdf.GetInfo(XDF_NTRI, &ntri);
	xdf.GetInfo(XDF_NEVENT, &nevt);
	xdf.GetInfo(XDF_SAMPLERATE, &sf);
	xdf.GetInfo(XDF_NSAMPLE, &nsamples);

	events = xdf.GetEvents();

	printf("Number of eeg:\t\t%d\n", neeg);
	printf("Number of exg:\t\t%d\n", nexg);
	printf("Number of tri:\t\t%d\n", ntri);
	printf("Number of events:\t%d\n", nevt);
	printf("Number of samples:\t%d\n", nsamples);
	printf("Samplerate:\t\t%d\n", sf);

	Eigen::MatrixXd eeg;
	
	unsigned int i = 0;
	while(i < 2) {

		ns = xdf.Read(fsize);
		nstot += ns;

		eeg = Eigen::Map<Eigen::MatrixXd>(xdf.Get(XDF_EEG), neeg, fsize);
		std::cout<<"=== Sample "<<i<<"==="<<std::endl;
		std::cout<<eeg<<std::endl;
		i++;
	}


	printf("Number of samples read: %d\n", nstot);


	for(auto i = 0; i<nevt; i++) {
		printf("[Event:%d] - Type|%d - Position|%d - Duration|%d\n", 
			i+1, events[i].typ, events[i].pos, events[i].dur);
	}

	xdf.Close();

	return 0;
}
