#include <fstream>

#include "wtk_io_utilities.hpp"


using namespace std;
using namespace wtk::io;

int main(int argc, char** argv) {

	ifstream ifs;
	ofstream ofs;

	hdr w_generic_header = {"TEST1", "0.0.1"};
	hdr r_generic_header;

	// Writing header
	ofs.open("./test_generic_header.dat", std::ios::binary);
	wtk_write_header(ofs, w_generic_header);
	printf("[write] - header.format  = %s\n", w_generic_header.format);
	printf("[write] - header.version = %s\n", w_generic_header.version);
	ofs.close();

	ifs.open("./test_generic_header.dat", std::ios::in | std::ios::binary);
	wtk_read_header(ifs, r_generic_header);
	printf("[read] - header.format  = %s\n", r_generic_header.format);
	printf("[read] - header.version = %s\n", r_generic_header.version);
	ifs.close();

	return 0;
}

