#include <fstream>

#include "wtk_io_utilities.hpp"

#define NUINT   10
#define NDOUBLE 5

using namespace std;
using namespace wtk::io;

int main(int argc, char** argv) {


	ifstream ifs;
	ofstream ofs;

	unsigned int t_array_uint[NUINT]     = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	double 	     t_array_double[NDOUBLE] = {1.23, 2.34, 3.45, 4.56, 5.67};
	char 	     t_array_char[]          = {"hello world"};

	unsigned int* r_array_uint;
	double*       r_array_double;
	char*         r_array_char;
	unsigned int  size_array_uint;
	unsigned int  size_array_double;
	unsigned int  size_array_char;

	// Writing
	ofs.open("./test_array_uint.dat", std::ios::binary);
	wtk_write_array(ofs, t_array_uint, NUINT);
	printf("[uint] - Wrote array (%d) on file: [", NUINT);
	for(auto i=0; i<NUINT; i++)
		printf("%d ", t_array_uint[i]);
	printf("\b]\n");
	ofs.close();

	ofs.open("./test_array_double.dat", std::ios::binary);
	wtk_write_array(ofs, t_array_double, NDOUBLE);
	printf("[double] - Wrote array (%d) on file: [", NDOUBLE);
	for(auto i=0; i<NDOUBLE; i++)
		printf("%f ", t_array_double[i]);
	printf("\b]\n");
	ofs.close();

	ofs.open("./test_array_char.dat", std::ios::binary);
	wtk_write_array(ofs, t_array_char, strlen(t_array_char));
	printf("[char] - Wrote array (%lu) on file: [", strlen(t_array_char));
	for(auto i=0; i<strlen(t_array_char); i++)
		printf("%c", t_array_char[i]);
	printf("]\n");
	ofs.close();

	// Reading
	ifs.open("./test_array_uint.dat", std::ios::in | std::ios::binary);
	wtk_read_array(ifs, r_array_uint, size_array_uint);
	printf("[uint] - Reading array (%d) from file: [", size_array_uint);
	for(auto i=0; i<size_array_uint; i++)
		printf("%d ", r_array_uint[i]);
	printf("\b]\n");
	ifs.close();
	
	ifs.open("./test_array_double.dat", std::ios::in | std::ios::binary);
	wtk_read_array(ifs, r_array_double, size_array_double);
	printf("[double] - Reading array (%d) from file: [", size_array_double);
	for(auto i=0; i<size_array_double; i++)
		printf("%f ", r_array_double[i]);
	printf("\b]\n");
	ifs.close();

	ifs.open("./test_array_char.dat", std::ios::in | std::ios::binary);
	wtk_read_array(ifs, r_array_char, size_array_char);
	printf("[char] - Reading array (%d) from file: [", size_array_char);
	for(auto i=0; i<size_array_char; i++)
		printf("%c", r_array_char[i]);
	printf("]\n");
	ifs.close();
	return 0;
}

