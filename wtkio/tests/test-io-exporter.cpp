#include "HDF5File.hpp"

#define SDATA_1 	10
#define SDATA_2		6
#define SDATA_3 	12

using namespace wtk::io;
int main(int argc, char** argv) {

	std::string name1	= "data1";
	double 	data1[SDATA_1]  = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10};
	uint32_t rank1 		= 1;
	hsize_t dims1[rank1];
	dims1[0] = SDATA_1;

	std::string name2	= "data2";
	int 	data2[SDATA_2] 	= {0, 1, 2, 3, 4, 5};
	uint32_t rank2 		= 2;
	hsize_t dims2[rank2];
	dims2[0] = 2;
	dims2[1] = 3;

	
	std::string name3	= "data3";
	float 	data3[SDATA_3] 	= {0.1, 1.2, 2.3, 3.4, 4.5, 5.6, 6.7, 7.8, 8.9, 9.10, 10.11, 11.12};
	uint32_t rank3 		= 3;
	hsize_t dims3[rank3];
	dims3[0] = 2;
	dims3[1] = 3;
	dims3[2] = 2;
	
	HDF5File exporter(H5std_string("./test_exporter.h5"));


	exporter.Export(data1, dims1, rank1, H5::PredType::NATIVE_DOUBLE, name1);
	exporter.Export(data2, dims2, rank2, H5::PredType::NATIVE_INT, name2);
	exporter.Export(data3, dims3, rank3, H5::PredType::NATIVE_FLOAT, name3);

	return 0;
}
