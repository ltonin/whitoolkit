#ifndef XDF_READER_CPP
#define XDF_READER_CPP


#include "XDFReader.hpp"

namespace wtk {
	namespace io {

XDFReader::XDFReader(void) {
	this->xdf_ 	= NULL;
	this->xdfch_ 	= NULL;
	this->neeg_ 	= 0;
	this->nexg_ 	= 0;
	this->ntri_ 	= 0;
	this->strides_  = NULL;
	this->sf_ 	= 0;
	this->nevt_ 	= 0;
}

XDFReader::~XDFReader(void) {
}

int XDFReader::Open(const char* filename) {

	int retcode = 0;

	try {
		/* NB: workaround to get number of samples, because
		 * xdf_seek seems to not properly work */
		this->open_xdf_file(filename);
		this->nsample_ = this->get_xdf_nsample();
		this->close_xdf_file();

		/* Normal xdf file opening */
		this->open_xdf_file(filename);
	} catch(std::runtime_error& e) {
		retcode = -1;
		handle_error(e.what());
	}
	return retcode;
}



int XDFReader::Close(void) {
	return this->close_xdf_file();
}

int XDFReader::Read(int size) {

	int ns = 0;

	/* Allocate memory for buffers */
	this->eeg_ = (double*)calloc(size*this->neeg_, sizeof(double*));
	this->exg_ = (double*)calloc(size*this->nexg_, sizeof(double*));
	this->tri_ = (uint32_t*)calloc(size*this->ntri_, sizeof(uint32_t*));

	/* Set memory values to 0 */
	memset(this->eeg_, 0, size*this->neeg_);
	memset(this->exg_, 0, size*this->nexg_);
	memset(this->tri_, 0, size*this->ntri_);


	ns = xdf_read(this->xdf_, size, this->eeg_, this->exg_, this->tri_);

	if (ns < 0) {
		handle_error(errno);
	}

	return ns;
}


double* XDFReader::Get(XDFtypes type) {
	
	double* dataptr;

	switch(type) {
		case XDF_EEG:
			dataptr = this->eeg_;
			break;
		case XDF_EXG:
			dataptr = this->exg_;
			break;
		default:
			handle_error("No such a type is available");
			dataptr = NULL;
			break;
	}

	return dataptr;
}

uint32_t* XDFReader::GetTrigger(void) {
	return this->tri_;
}

xdfevent* XDFReader::GetEvents(void) {
	return this->events_;
}

// XDF methods //
int XDFReader::open_xdf_file(const char* filename) {

	int retcode = 0;

	this->xdf_ = xdf_open(filename, XDF_READ, XDF_ANY);

	if (this->xdf_ == NULL) { 
		throw std::runtime_error("Error opening xdf file");
		retcode = -1;
	} else {
		retcode = this->read_xdf_header(); 
		retcode = this->init_xdf_structure();
		retcode = this->read_xdf_events();
	}

	return retcode;
}

int XDFReader::close_xdf_file(void) {
	int retcode;

	if(retcode = xdf_close(this->xdf_) < 0)
		handle_error(errno);

	return retcode;
}

int XDFReader::get_xdf_nsample(void) {
	
	int ns = 1, nstot = 0;

	while(ns > 0) {
		ns = this->Read(1);
		nstot += ns;
	}

	return nstot;
}

int XDFReader::read_xdf_header(void) {

	int retcode;

	if(retcode = this->get_xdf_format()< 0)
	   	handle_error("[XDFReader] Error getting format information");

	if(retcode = this->get_xdf_nchannel()< 0)
	   	handle_error("[XDFReader] Error getting channels information");

	if(retcode = this->get_xdf_samplerate()< 0)
	   	handle_error("[XDFReader] Error getting samplerate information");

	if(retcode = this->get_xdf_nevent()< 0)
		handle_error("[XDFReader] Error getting events information");

	return retcode;
}

int XDFReader::get_xdf_format(void) {
	
	int retcode;
	
	if(retcode = xdf_get_conf(this->xdf_, XDF_F_FILEFMT, &this->format_, XDF_NOF) < 0)
		handle_error(errno);

	return retcode;
}

int XDFReader::get_xdf_nchannel(void) {

	int retcode;
	
	if(retcode = xdf_get_conf(this->xdf_, XDF_F_NCHANNEL, &this->nch_, XDF_NOF) < 0)
		handle_error(errno);

	switch(this->format_) {
		case XDF_BDF:
			this->nexg_ = 24;
			this->ntri_ = 1;
			break;
		case XDF_GDF1:
			this->nexg_ = 0;
			this->ntri_ = 1;
			break;
		case XDF_GDF2:
			this->nexg_ = 0;
			this->ntri_ = 1;
			break;
		default:
			handle_error("Not recognize xdf format (only BDF, GDF1 and GDF2)");
			retcode = -1;
			break;
	}

	this->neeg_ = this->nch_ - this->nexg_ - this->ntri_;

	return retcode;
}

int XDFReader::get_xdf_samplerate(void) {
	int retcode;
	
	if(retcode = xdf_get_conf(this->xdf_, XDF_F_SAMPLING_FREQ, &this->sf_, XDF_NOF) < 0)
		handle_error(errno);

	return retcode;
}

int XDFReader::get_xdf_nevent(void) {

	int retcode;

	return retcode;
}

int XDFReader::init_xdf_structure(void) {
	
	int retcode = 0;
	int offset;
	unsigned int i;

	this->strides_ = (size_t*)malloc(3*sizeof(size_t));

	for (i=0; i<this->nch_; i++) {
		this->xdfch_ = xdf_get_channel(this->xdf_, i);
			
		if(this->xdfch_ == NULL) {
			handle_error(errno);
			retcode = -1;
		}

		if(retcode = xdf_set_chconf(this->xdfch_, XDF_CF_ARRINDEX, -1, XDF_NOF) < 0)
			handle_error(errno);
	}
	
	// Configure the channels requested
	offset = 0;
	for (i=0; i<this->neeg_; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->xdf_, i), 
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 0,
						XDF_CF_ARRTYPE, XDFDOUBLE,
						XDF_CF_ARROFFSET, offset, 
						XDF_NOF) < 0) 
			handle_error(errno);

			
		offset += sizeof(double);
	}
	this->strides_[0] = offset;
	
	offset = 0;
	for (i=0; i<this->nexg_; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->xdf_, i+this->neeg_),
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 1,
						XDF_CF_ARRTYPE, XDFDOUBLE,
						XDF_CF_ARROFFSET, offset,
						XDF_NOF) < 0)
			handle_error(errno);


		offset += sizeof(double);
	}
	this->strides_[1] = offset;

	offset = 0;
	for (i=0; i<this->ntri_; i++) {
		if(retcode = xdf_set_chconf(xdf_get_channel(this->xdf_, i+this->neeg_+this->nexg_),
						XDF_CF_ARRDIGITAL, 0,
						XDF_CF_ARRINDEX, 2,
						XDF_CF_ARRTYPE, XDFINT32,
						XDF_CF_ARROFFSET, offset,
						XDF_NOF) < 0)
			handle_error(errno);
		
		offset += sizeof(int32_t);
	}
	this->strides_[2] = offset;

	// Define arrays and prepare xdf transfer
	if (xdf_define_arrays(this->xdf_, 3, this->strides_)
	    || xdf_prepare_transfer(this->xdf_)) {
		handle_error(errno);
		retcode = -1;
	}

	
	return retcode;
}


// EVENT methods //
int XDFReader::read_gdf_events(void) {

	int nevttot;
	int nevttyp;

	if(xdf_get_conf(this->xdf_, XDF_F_NEVENT, &nevttot, XDF_F_NEVTTYPE, &nevttyp, XDF_NOF))
		handle_error(errno, "Error reading events from xdf");

	unsigned int evt_typ, off_typ;
	int evt_code, off_code;
	const char* evt_desc[256];
	const char* off_desc[256];
	double evt_onset, evt_dur, off_onset, off_dur;

	unsigned int offset_evt_id = 0;
	bool 	     offset_evt_found = false;

	int typ;
	unsigned int pos;
	unsigned int dur;

	xdfevent events[nevttot];
	unsigned int evt_set_id = 0;

	for(auto i = 0; i<nevttot; i++) {
		// Get the first event
		xdf_get_event(this->xdf_, i, &evt_typ, &evt_onset, &evt_dur);
		xdf_get_evttype(this->xdf_, evt_typ, &evt_code, evt_desc);
		
		offset_evt_found = false;

		// Iterate over the rest of the events looking for the first OFF
		// event
		for(auto u = i; u<nevttot; u++) {
			xdf_get_event(this->xdf_, u, &off_typ, &off_onset, &off_dur);
			xdf_get_evttype(this->xdf_, off_typ, &off_code, off_desc);
			if(off_code == (evt_code + GDF_EVENT_OFF)) {
				offset_evt_id    = u;
				offset_evt_found = true;
				break;
			}
		}

		// Extract type, position and duration of the event	
		// Compute duration with respect to event onset (if off event is
		// found), otherwise set 0
		typ = evt_code;
		pos = (unsigned int)round(evt_onset*this->sf_);
		dur = offset_evt_found ? (unsigned int)round((off_onset - evt_onset)*this->sf_) : 0;

		// If the current event (id) is different from the last found
		// offset event id then save event in the array structure
		if (i != offset_evt_id) {
			events[evt_set_id] = {typ, pos, dur};
			evt_set_id++;
		}
	}
	
	this->nevt_   = evt_set_id;
	this->events_ = (xdfevent*)calloc(this->nevt_, sizeof(xdfevent)); 
	std::copy(events, events + this->nevt_, this->events_);
	
}

int XDFReader::read_bdf_events(void) {

	this->nevt_   = 0;
	this->events_ = NULL; 
}

int XDFReader::read_xdf_events(void) {

	switch(this->format_) {
		case XDF_BDF:
			this->read_bdf_events();
			break;
		case XDF_GDF2:
			this->read_gdf_events();
			break;
	}
}

// Handle error functions //
void XDFReader::handle_error(int nerr, std::string msg) {

	std::string objname = "XDFReader";

	if (msg.empty() == false)
		msg = ": " + msg;
	
	fprintf(stderr, "[%s] - (%i) %s%s\n", objname.c_str(), nerr, strerror(nerr), msg.c_str()); 
}

void XDFReader::handle_error(std::string msg) {

	std::string objname = "XDFReader";
	fprintf(stderr, "[%s] - %s\n", objname.c_str(), msg.c_str()); 
}
	}
}
#endif
