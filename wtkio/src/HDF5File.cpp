#ifndef HDF5FILE_CPP
#define HDF5FILE_CPP

#include "HDF5File.hpp"

namespace wtk {
	namespace io {

HDF5File::HDF5File(std::string filename, uint32_t mode) {

	this->filename_ = filename;
	this->mode_ 	= mode;
	
	try {
		this->hd5_open_file();
	} catch (H5::FileIException e) {
		e.printError();
	}
}

HDF5File::~HDF5File(void) {

	this->file_->close();
}


void HDF5File::hd5_open_file(void) {
	this->file_ = new H5::H5File(this->filename_, this->mode_);
}

void HDF5File::hd5_create_dataspace(hsize_t* dims, int rank) {
	this->dataspace_ = new H5::DataSpace(rank, dims);
}

void HDF5File::hd5_create_dataset(std::string name, H5::PredType type) {
	this->dataset_ = new H5::DataSet(this->file_->createDataSet(name, type, *this->dataspace_));
}


	}
}

#endif
