#ifndef HDF5File_HPP
#define HDF5File_HPP

#include <string>
#include <H5Cpp.h>

namespace wtk {
	namespace io {

class HDF5File {
	public:
		HDF5File(std::string filename, uint32_t mode = H5F_ACC_TRUNC);
		virtual ~HDF5File(void);

		template<typename T>
		int Export(T* data, hsize_t* dim, int rank, H5::PredType type, std::string name);

		/*** TODO ***/
		void Import(void) {};


	private:
		void hd5_open_file(void);
		void hd5_create_dataspace(hsize_t* dims, int rank);
		void hd5_create_dataset(std::string name, H5::PredType type);

	private:
		std::string 	filename_;
		H5::H5File* 	file_;
		uint32_t 	mode_;

		H5::DataSpace* 	dataspace_;
		H5::DataSet*	dataset_;

};

template<typename T>
int HDF5File::Export(T* data, hsize_t* dims, int rank, H5::PredType type, std::string name) {

	this->hd5_create_dataspace(dims, rank);

	this->hd5_create_dataset(name, type);

	this->dataset_->write(data, type);

}

	}
}


#endif
