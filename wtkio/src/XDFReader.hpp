#ifndef XDF_READER_HPP
#define XDF_READER_HPP

#include <string>
#include <cstring>
#include <cmath>
#include <xdfio.h>
#include <stdexcept>

#define GDF_EVENT_OFF	32768

namespace wtk {
	namespace io {

struct xdfevent {
	int		typ;
	unsigned int 	pos;
	unsigned int	dur;
};

enum XDFtypes {
	XDF_EEG,
	XDF_EXG,
	XDF_TRI
};

enum XDFfield {
	XDF_NEEG, 
	XDF_NEXG,
	XDF_NTRI,
	XDF_NSAMPLE,
	XDF_SAMPLERATE,
	XDF_NEVENT,
	XDF_FORMAT
};

class XDFReader {

	public:
		XDFReader(void);
		~XDFReader(void);

		int Open(const char* filname);
		int Close(void);

		template <typename T>
		int GetInfo(XDFfield field, T* value);


		int Read(int size);

		double*   Get(XDFtypes type = XDF_EEG);
		uint32_t* GetTrigger(void);
		xdfevent* GetEvents(void);

	private:
		int open_xdf_file(const char* filename);
		int close_xdf_file(void);
		int init_xdf_structure(void);
		int read_xdf_header(void);

		int get_xdf_nsample(void);
		int get_xdf_format(void);
		int get_xdf_nchannel(void);
		int get_xdf_nevent(void);
		int get_xdf_samplerate(void);
		
		int read_xdf_events(void);
		int read_bdf_events(void);
		int read_gdf_events(void);

		void handle_error(int nerr, std::string msg = "");
		void handle_error(std::string msg);

	private:
		// XDF structures
		struct xdf*	xdf_;
		struct xdfch*	xdfch_;

		// Data buffers
		double*   eeg_;
		double*   exg_;
		uint32_t* tri_;
		size_t *  strides_;

		// Header information
		xdffiletype format_;
		int sf_;
		int nch_;
		int neeg_;
		int nexg_;
		int ntri_;
		int nsample_;
		int nevt_;

		// Events structure
		xdfevent* events_;
};

template <typename T>
int XDFReader::GetInfo(XDFfield field, T* value) {

	switch(field) {
		case XDF_NEEG:
			*value = (T)this->neeg_;
			break;
		case XDF_NEXG:
			*value = (T)this->nexg_;
			break;
		case XDF_NTRI:
			*value = (T)this->ntri_;
			break;
		case XDF_NSAMPLE:
			*value = (T)this->nsample_;
			break;
		case XDF_SAMPLERATE:
			*value = (T)this->sf_;
			break;
		case XDF_NEVENT:
			*value = (T)this->nevt_;
			break;
		case XDF_FORMAT:
			*value = (T)this->format_;
			break;
	}
}
	}
}
#endif
