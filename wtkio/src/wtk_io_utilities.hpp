#ifndef WTK_IO_UTILITIES_HPP
#define WTK_IO_UTILITIES_HPP

#include <fstream>
#include <stdint.h>
#include <stdlib.h>
#include <cstring>
#include <string>

#define BYTES_U16 2
#define BYTES_U32 4

#define WTK_FORMAT	"WHITK"
#define WTK_VERSION	"1.0.0"

#define WTK_HDR_FORMAT_LENGHT	16
#define WTK_HDR_VERSION_LENGHT	16

#define WTK_BLK_NAME_SIZE	32

namespace wtk {
	namespace io {

enum blktype { WTK_INT16, WTK_INT32, WTK_INT64, WTK_UINT16, WTK_UINT32, WTK_UINT64, WTK_CHAR, WTK_DOUBLE, WTK_FLOAT };

typedef uint32_t byte_t;

struct hdr {
	char 	format[WTK_HDR_FORMAT_LENGHT];
	char 	version[WTK_HDR_VERSION_LENGHT];
};

template<typename T=double>
struct block {
	unsigned int 	btype;
	unsigned int 	stype;
	unsigned int 	rank;
	unsigned int*	dim;
	char 		name[WTK_BLK_NAME_SIZE];
	unsigned int 	nelem;
	T* 		data;
};

// Read a byte
template<typename T=wtk::io::byte_t>
std::istream& wtk_read_byte(std::ifstream& ifs, T& value);

// Write a byte
template<typename T=wtk::io::byte_t>
std::ostream& wtk_write_byte(std::ofstream& ofs, T& value);

// Read array
template<typename T>
std::istream& wtk_read_array(std::ifstream& ifs, T*& array, unsigned int& size);

// Write array
template<typename T>
std::ostream& wtk_write_array(std::ofstream& ofs, T& array, unsigned int size);

// Read generic header
std::istream& wtk_read_header(std::ofstream& ofs, hdr& header);

// Write generic header
std::ostream& wtk_write_header(std::ofstream& ofs, hdr& header);

size_t wtk_get_sizeof(blktype type);

// Write block data
template <typename T>
std::ostream& wtk_write_block(std::ofstream& ofs, block<T>& blk);

// Reading block data
template <typename T>
std::istream& wtk_read_block(std::ifstream& ifs, block<T>& blk);

	}
}

#include "wtk_io_utilities.cpp"

#endif
