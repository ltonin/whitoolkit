#ifndef WTK_IO_UTILITIES_CPP
#define WTK_IO_UTILITIES_CPP

namespace wtk {
	namespace io {

template<typename T>
std::istream& wtk_read_byte(std::ifstream& ifs, T& value) {
	return ifs.read(reinterpret_cast<char*>(&value), sizeof(T));
}	

template<typename T>
std::ostream& wtk_write_byte(std::ofstream& ofs, T& value) {
	return ofs.write(reinterpret_cast<const char*>(&value), sizeof(T));
}

template<typename T>
std::istream& wtk_read_array(std::ifstream& ifs, T*& array, unsigned int& size) {

	// Read first byte: array size
	wtk_read_byte(ifs, size);

	// Allocate memory for input array
	array = new T[size];

	// Reading array values from next bytes
	for(auto i=0; i<size; i++)
		wtk_read_byte(ifs, array[i]);

	//return ifs;
}

template<typename T>
std::ofstream& wtk_write_array(std::ofstream& ofs, T* array, unsigned int size) {

	// First byte: array size
	wtk_write_byte(ofs, size);

	// Next bytes: array values
	for(auto i=0; i<size; i++)
		wtk_write_byte(ofs, array[i]);

	return ofs;
}

std::istream& wtk_read_header(std::ifstream& ifs, hdr& header) {

	unsigned int sformat;
	unsigned int sversion;
	char *format, *version;

	// Read header format
	wtk_read_array(ifs, format, sformat);
	
	// Read header version
	wtk_read_array(ifs, version, sversion);

	for(auto i=0; i<sformat; i++)
		header.format[i] = format[i];
	
	for(auto i=0; i<sversion; i++)
		header.version[i] = version[i];

	return ifs;
}

std::ostream& wtk_write_header(std::ofstream& ofs, hdr& header) {
	unsigned int sformat  = WTK_HDR_FORMAT_LENGHT;
	unsigned int sversion = WTK_HDR_VERSION_LENGHT;

	// Write header format
	wtk_write_array(ofs, header.format, sformat); 
	
	// Write header version
	wtk_write_array(ofs, header.version, sversion); 

	return ofs;
}

template <typename T>
std::ostream& wtk_write_block(std::ofstream& ofs, block<T>& blk) {

	// Write block type
	ofs = wtk_write_byte(ofs, blk.btype);

	// Write size of type
	ofs = wtk_write_byte(ofs, blk.stype);
	
	// Write rank of data
	ofs = wtk_write_byte(ofs, blk.rank);

	// Write dimensions of data
	ofs = wtk_write_byte(ofs, blk.dim);

	// Write name of the data
	ofs = wtk_write_array(ofs, blk.name);
	
	// Write number of element in the data
	ofs = wtk_write_byte(ofs, blk.nelem);

	// Write data block
	ofs = wtk_write_array(ofs, blk.data, blk.nelem);

	return ofs;
}

template<typename T>
std::istream& wtk_read_block(std::ifstream& ifs, block<T>& blk) {

	// Read block type
	ifs = wtk_read_byte(ifs, blk.btype);

	// Read size of type
	ifs = wtk_read_byte(ifs, blk.stype);
	
	// Read rank of data
	ifs = wtk_read_byte(ifs, blk.rank);

	// Read dimensions of data
	ifs = wtk_read_byte(ifs, blk.dim);

	// Read name of the data
	ifs = wtk_read_array(ifs, blk.name);
	
	// Read number of element in the data
	ifs = wtk_read_byte(ifs, blk.nelem);

	// Read data block
	ifs = wtk_read_array(ifs, blk.data, blk.nelem);

	return ifs;

}


size_t wtk_get_sizeof(blktype type) {
	size_t size;

	switch(type) {
		case WTK_INT64:
			size = sizeof(int64_t);
			break;
		case WTK_INT32:
			size = sizeof(int32_t);
			break;
		case WTK_INT16:
			size = sizeof(int16_t);
			break;
		case WTK_UINT64:
			size = sizeof(uint64_t);
			break;
		case WTK_UINT32:
			size = sizeof(uint32_t);
			break;
		case WTK_UINT16:
			size = sizeof(uint16_t);
			break;
		case WTK_CHAR:
			size = sizeof(char);
			break;
		case WTK_DOUBLE:
			size = sizeof(double);
			break;
		case WTK_FLOAT:
			size = sizeof(float);
			break;
		default:
			size = 0;
			break;
	}
	return size;
}

	

	}
}

#endif
