#!/bin/bash

PrintTitle() {
	STYLE_ON='\e[35m\e[1m'
	STYLE_OFF='\e[21m\e[39m'
	echo -e $STYLE_ON$1$STYLE_OFF
}

PrintError() {
	STYLE_ON='\e[31m\e[1m'
	STYLE_OFF='\e[21m\e[39m'
	echo -e $STYLE_ON$1$STYLE_OFF
}

Usage() {
	echo "[wtkapp::install] - Post installation usage:\n"
	echo "sh post-installation.sh [-s SRC=./extra, -d DEST=$HOME/.whitk/]\n"
}


DST_EXTRA_DIR=$HOME/.whitk/
SRC_EXTRA_DIR=./extra/

while getopts ":d:s:" opt; do
  case $opt in
    d)
      DST_EXTRA_DIR=$OPTARG
      ;;
    s)
      SRC_EXTRA_DIR=$OPTARG
      ;;
    \?)
      echo "[wtkapp::install] - Error. Invalid option: -$OPTARG" >&2
      Usage;
      exit 1
      ;;
  esac
done


echo "[wtkapp::install] - Source folder: $SRC_EXTRA_DIR" >&2
echo "[wtkapp::install] - Destination folder: $DST_EXTRA_DIR " >&2


########### Checking and creating directories ###########


echo "[wtkapp::install] - Creating configuration directory tree"

if [ ! -d "$DST_EXTRA_DIR" ]; then
	echo "   [mkdir] - Creating configuration directory: $DST_EXTRA_DIR"
	mkdir -m 755 $DST_EXTRA_DIR
else
	echo "   [mkdir] - Configuration directory $DST_EXTRA_DIR already exists"
fi

###################################################

########### Copy configuration files ###########
echo "[wtkapp::install] - Install configuration files"

cp -r $SRC_EXTRA_DIR/* $DST_EXTRA_DIR
chmod -R 0755 $DST_EXTRA_DIR/* 

###################################################
