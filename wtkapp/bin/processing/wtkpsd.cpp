#include <string>
#include <getopt.h>
#include <vector>

#include <Eigen/Dense>
#include <wtkio/XDFReader.hpp>
#include <wtkprocessing/Buffer.hpp>
#include <wtkprocessing/RingBuffer.hpp>
#include <wtkprocessing/Filter.hpp>
#include <wtkprocessing/Car.hpp>
#include <wtkprocessing/Laplacian.hpp>
#include <wtkprocessing/Pwelch.hpp>
#include <wtkio/HDF5File.hpp>

#include "wtk_app_utilities.hpp"

#define DEFAULT_DUMP_FLAG		0
#define DEFAULT_FRAME_SIZE		32
#define DEFAULT_BUFFER_TYPE		"ringbuffer"
#define DEFAULT_BUFFER_SIZE		512
#define DEFAULT_SPATIAL_TYPE		"laplacian"
#define DEFAULT_SPATIAL_LAP_MASK 	".whitk/data/laplacian_mask16ch.dat"
#define DEFAULT_PWELCH_WSIZE		256
#define DEFAULT_PWELCH_NOVL		128

using namespace std;
using namespace Eigen;
using namespace wtk::io;
using namespace wtk::proc;
using namespace wtk::app;

size_t frame_size, buffer_size, pwelch_wsize, pwelch_novl, pwelch_nfft;
string	spatial_type, buffer_type, spatial_lap_mask;

unsigned int nfiles;
int fileId;

void proc_setup_buffer(Buffer*& buffer, std::string type, size_t bsize, size_t nch) {

	if(type.compare("ringbuffer") == 0) {
		buffer = new RingBuffer();
		buffer->Setup(bsize, nch);
	} else {
		throw std::runtime_error("[wtkpsd] - Unknown requested buffer type");
	}
}

void proc_setup_filter(Filter*& filter, std::string type, string mask = "") {

	if(type.compare("car") == 0) {
		filter = new Car();
	} else if(type.compare("laplacian") == 0) {
		if(mask.empty() == true)
			throw std::runtime_error("[wtkpsd] - Provide a path for laplacian mask");
		filter = new Laplacian(mask.c_str());
	} else {
		throw std::runtime_error("[wtkpsd] - Unknown requested spatial filter type");
	}
}

void proc_setup_pwelch(Pwelch*& pwelch, unsigned int wlength, unsigned int novl, unsigned int sf) {
	
	try {
		pwelch = new Pwelch();
		pwelch->Setup(wlength, Window::AsHamming, novl, sf, 0);
		pwelch_nfft = pwelch->config.nfft;
	} catch (runtime_error& e) {
		throw runtime_error(e.what());
	}
}

void proc_dump_configuration(void) {
	printf("[wtkpsd] - Processing parameters:\n");
	printf(" 	   TYPE|%s\tFRAME-SIZE|%lu\t\tBUFFER-SIZE|%lu\n", buffer_type.c_str(), frame_size, buffer_size);
	printf(" 	   TYPE|%s\t", spatial_type.c_str());
	if(spatial_type.compare("laplacian") == 0) printf("MASK|%s\n", spatial_lap_mask.c_str()); else printf("\n");
	printf("	   TYPE|pwelch\t\tWIN-SIZE|%lu\t\tWIN-OVL|%lu\n", pwelch_wsize, pwelch_novl);
}

void help(void) {

	printf("\n" WTKOUT_BOLD "SYNOPSYS\n\n" WTKOUT_RESET);
	printf("\t" WTKOUT_BOLD "wtkpsd" WTKOUT_RESET " <input files [.gdf|.bdf]> [OPTIONS]\n\n");
	printf(WTKOUT_BOLD "OPTIONS\n\n" WTKOUT_RESET);
	printf("\t--frame-size\t\tLength (in samples) of the frame read at each iteration (aka step-size)\n");
	printf("\t\t\t\t[DEFAULT: %d samples]\n", DEFAULT_FRAME_SIZE);
	printf("\t--buffer-size\t\tLength (in samples) of the ring buffer\n");
	printf("\t\t\t\t[DEFAULT: %d samples]\n", DEFAULT_BUFFER_SIZE);
	printf("\t--spatial-type\t\tType of the spatial filter [car|laplacian] [DEFAULT: %s] \n", DEFAULT_SPATIAL_TYPE);
	printf("\t--spatial-lap-mask\tPath to the laplacian mask (only for laplacian filter)\n"); 
	printf("\t\t\t\t[DEFAULT: $HOME/%s]\n", DEFAULT_SPATIAL_LAP_MASK);
	printf("\t--pwelch-wsize\t\tLength (in samples) of the pwelch window\n");
	printf("\t\t\t\t[DEFAULT: %d samples]\n", DEFAULT_PWELCH_WSIZE);
	printf("\t--pwelch-novl\t\tNumber of samples that overlap in pwelch window\n");
	printf("\t\t\t\t[DEFAULT: %d samples]\n", DEFAULT_PWELCH_NOVL);
	printf("\t--dump\t\tDump processing configuration\n");
	printf("\t\t\t\t[DEFAULT: %d]\n", DEFAULT_DUMP_FLAG);
	printf(WTKOUT_BOLD "EXAMPLES\n\n" WTKOUT_RESET);
	printf("\tCompute pwelch processing on all gdf files in the folder (samplerate of 512Hz and 16 channels).\n");
	printf("\tFrame size: 32 samples (0.0625 seconds); Buffer size: 512 samples (1 seconds);\n");
	printf("\tSpatial filter: car; Window size of pwelch: 256 samples (0.5 seconds);\n");
	printf("\tNumber of overlapping samples for pwelch: 128 samples (0.250 seconds)\n\n");
	printf("\twtkpsd *.gdf --frame-size=32 --buffer-size=512 --spatial-type=car --pwelch-wsize=256 --pwelch-novl=128\n\n");

	exit(0);
}

void parse_option_arguments(int argc, char** argv) {

	int c;
	std::string optstr;	
	int help_flag = 0;
	int dump_flag = DEFAULT_DUMP_FLAG;

	while(1) {
		static struct option long_opts[] = 
		{
			{"dump", 	     no_argument,       &dump_flag, 1},
			{"frame-size",       required_argument, 0, 'a'},
			{"buffer-size",      required_argument, 0, 'b'},
			{"spatial-type",     required_argument, 0, 'c'},
			{"spatial-lap-mask", required_argument, 0, 'd'},
			{"pwelch-wsize",     required_argument, 0, 'e'},
			{"pwelch-novl",      required_argument, 0, 'f'},
			{"help", 	     no_argument,       &help_flag, 1},
			{0, 0, 0, 0}
		};
		
		int opt_index = 0;
		c = getopt_long(argc, argv, "a:b:c:d:e:f:", long_opts, &opt_index);

		if (c == -1) break;

		switch(c) {
			case 0:
          			if (long_opts[opt_index].flag != 0)
            			break;
			case 'a':
				frame_size = std::stoi(optarg);
          			break;
			case 'b':
				buffer_size = std::stoi(optarg);
          			break;
			case 'c':
				spatial_type.assign(optarg);
          			break;
			case 'd':
				spatial_lap_mask.assign(optarg);
          			break;
			case 'e':
				pwelch_wsize = std::stoi(optarg);
          			break;
			case 'f':
				pwelch_novl = std::stoi(optarg);
          			break;
			default:
				help();
				break;
        		}		
		}
	if (dump_flag) {
    		printf("[wtkpsd] - Dumping processing configuration\n");
		proc_dump_configuration();
	}

	if (help_flag)
		help();
	
	nfiles = argc - optind;
	fileId = optind;
}


int main(int argc, char** argv) {
	
	/* Default arguments */
	frame_size 	 = DEFAULT_FRAME_SIZE;
	buffer_type 	 = DEFAULT_BUFFER_TYPE; 
	buffer_size 	 = DEFAULT_BUFFER_SIZE; 
	pwelch_wsize 	 = DEFAULT_PWELCH_WSIZE;
	pwelch_novl 	 = DEFAULT_PWELCH_NOVL;
	spatial_type 	 = DEFAULT_SPATIAL_TYPE;
	spatial_lap_mask = std::string(getenv("HOME")) + "/" + std::string(DEFAULT_SPATIAL_LAP_MASK);

	parse_option_arguments(argc, argv);

	XDFReader xdf;
	unsigned int ns, nstot, nch, sf;
	unsigned int nf, rs, fidx;
	std::string xdf_filename, psd_filename;
	xdfevent* events;
	int nevt;
	int* evttyp;
	unsigned int* evtpos;
	unsigned int* evtdur;

	// HDF5 objects
	HDF5File* 	exporter;
	double* 	psdraw;
	int 		psdrank = 3;
	hsize_t 	psddims[psdrank]; 	
	hsize_t 	evtdims[1];
	hsize_t 	griddims[1];

	// Frame variables
	double*  frame;
	unsigned int nframes;

	// Processing objects and parameters
	Buffer* buffer;
	Filter* filter;
	Pwelch* pwelch;
	
	// Processing eigen matrix
	MatrixXd raw_data;
	MatrixXd buff_data;
	MatrixXd filt_data;
	MatrixXd psd_data;
	MatrixXd psd;
	vector<uint32_t> fgrid;

	nf = 1;
	for (auto f = fileId; f < argc; f++) {
	
		xdf_filename.assign(argv[fileId++]);
		psd_filename = xdf_filename + ".psd";

		// Open xdf and getting information
		xdf.Open(xdf_filename.c_str());
		xdf.GetInfo(XDF_NEEG, &nch);
		xdf.GetInfo(XDF_SAMPLERATE, &sf);
		xdf.GetInfo(XDF_NSAMPLE, &nstot);

		// Getting events from XDF
		events = xdf.GetEvents();
		xdf.GetInfo(XDF_NEVENT, &nevt);	
		evtdims[0] = nevt;
		evttyp = (int*)calloc(nevt, sizeof(int*));
		evtpos = (unsigned int*)calloc(nevt, sizeof(unsigned int*));
		evtdur = (unsigned int*)calloc(nevt, sizeof(unsigned int*));
		for (auto i = 0; i<nevt; i++) {
			evttyp[i] = events[i].typ;
			evtpos[i] = floor(events[i].pos/frame_size);
			evtdur[i] = floor(events[i].dur/frame_size);
		}
		
		// Setup processing structure
		try {
			proc_setup_buffer(buffer, buffer_type,  buffer_size, nch);
			proc_setup_filter(filter, spatial_type, spatial_lap_mask);
			proc_setup_pwelch(pwelch, pwelch_wsize, pwelch_novl, sf);
		} catch (runtime_error& e) {
			fprintf(stderr, "[wtkpsd] - %s\n",  e.what());
			exit(1);
		}

		nframes = (round(nstot/frame_size));
		psddims[0] = nch; 
		psddims[1] = nframes;
		psddims[2] = pwelch_nfft;
		fgrid 	   = pwelch->config.grid;
		griddims[0]= pwelch_nfft;
		

		// Setup eigen matrix
		buff_data = MatrixXd::Zero(buffer_size, nch);
		filt_data = MatrixXd::Zero(buffer_size, nch);
		psd_data  = MatrixXd::Zero(pwelch_nfft, nch);
		psd       = MatrixXd::Zero(nframes*pwelch_nfft, nch);

		// Start processing loop
		printf("[wtkpsd] - Start processing file %d/%d: %s\n", nf++, nfiles, xdf_filename.c_str());
		printf("           [EEG samples: %d; EEG channels: %d; Samplerate: %d Hz; NFFT: %lu]\n", nstot, nch, sf, pwelch_nfft);
	
		exporter = new HDF5File(psd_filename);

		ns = 1;	
		rs = 0;
		while(ns > 0) {
			fidx = round(rs/frame_size);
			wtk_progress_bar(fidx, nframes);

			ns = xdf.Read(frame_size);

			if (ns <= 0)
				break;

			rs += ns;
			frame = xdf.Get(XDF_EEG);
			
			raw_data = Map<MatrixXd>(frame, nch, frame_size);
			raw_data.transposeInPlace();
			
			buffer->Add(raw_data);

			if(buffer->IsFull() == false)
				continue;

			buffer->Get(buff_data);

			filter->Apply(buff_data, filt_data);

			pwelch->Apply(filt_data);
				
			pwelch->Get(psd_data);
		
			psd.middleRows(fidx*pwelch_nfft, pwelch_nfft) = psd_data;


		}

		
		xdf.Close();

		psdraw = &psd(0);
		exporter->Export(psdraw, psddims, psdrank, H5::PredType::NATIVE_DOUBLE, "psd");
		exporter->Export(evttyp, evtdims, 1, H5::PredType::NATIVE_INT,  "evttyp");
		exporter->Export(evtpos, evtdims, 1, H5::PredType::NATIVE_UINT, "evtpos");
		exporter->Export(evtdur, evtdims, 1, H5::PredType::NATIVE_UINT, "evtdur");
		exporter->Export(&fgrid[0], griddims, 1, H5::PredType::NATIVE_UINT, "frequencies");

		printf("Frequency grid:\n[");
		for(auto i=0; i<fgrid.size(); i++)
			printf("%d ", fgrid.at(i));

		printf("\r]\n");
	}

	return 0;
}
