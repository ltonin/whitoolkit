#ifndef SMR_INC_ONLINE_CPP
#define SMR_INC_ONLINE_CPP

#include <stdlib.h> 
#include <cmath>
#include <drawtk.h>

#include <cnbiloop/ClLoop.hpp>
#include <cnbicore/CcLog.hpp>

#include <wtkcore/Core.hpp>
#include <wtkcore/Time.hpp>
#include <wtkcore/wtk_random_utilities.hpp>
#include <wtkdraw/Bar.hpp>
#include <wtkdraw/Cross.hpp>
#include <wtkdraw/Circle.hpp>
#include <wtkdraw/DrawEngine.hpp>

#include <wtknet/IFaceC.hpp>
#include <wtknet/IFaceD.hpp>

#include <wtkprotocol/TaskSet.hpp>
#include <wtkprotocol/Timings.hpp>
#include <wtkprotocol/Events.hpp>
#include <wtkprotocol/wtk_protocol_utilities.hpp>

#include "smr-inc_utilities.hpp"

using namespace std;
using namespace wtk::core;
using namespace wtk::draw;
using namespace wtk::protocol;
using namespace wtk::net;


void usage(void) {
	printf("\nUsage: smr-inc_online -x XML_PROTOCOL [OPTIONS]...\n");
	printf("\n Options:");
	printf("\n  -e	XML_EVENTS	Path to xml file with events 	[Default: $HOME/.whitk/xml/wtk_events.xml]");
	printf("\n  -p	PIPENAME 	iC pipe name		[Default: '/ctrl0']");
	printf("\n  -b	BUSNAME		iD bus name 		[Default: '/bus']");
	printf("\n  -f			Fullscreen 		[Default: 1024x768]");
	printf("\n\n");
}

int main (int argc, char** argv) {

	int opt;
	std::string xml_protocol;
	std::string xml_events = SMR_INC_XML_EVENTS;
	unsigned int wsize[2]  = {SMR_INC_WIN_WIDTH, SMR_INC_WIN_HEIGHT};
	std::string icpipe = "/ctrl0";
	std::string idpipe = "/bus";

	while((opt = getopt(argc, argv, "x:e:p:b:f")) != -1) {
		switch(opt) {
			case 'x':
				xml_protocol.assign(optarg);
				break;
			case 'e':
				xml_events.assign(optarg);
				break;
			case 'p':
				icpipe.assign(optarg);
				break;
			case 'b':
				idpipe.assign(optarg);
				break;
			case 'f':
				wsize[0] = 0;
				wsize[1] = 0;
				break;
			default:
				usage();
				return -1;
		}
	}

	unsigned int idx;
	float Period;
	float F;
	float A;
	float value;
	float npp;
	int fidx = TCBlock::BlockIdxUnset;
	
	TaskSet taskset;	
	Timings timings;
	Events  events;
	TrialInfo info;
	IFaceC 	 ic(IFaceC::AsReceiver);
	ICMessage* icmsg;
	IFaceD   id(IFaceD::AsSender);

	/***** CNBI Loop setup *****/
	CcCore::OpenLogger("smr-inc_online");
	ClLoop::Configure();
	if(ClLoop::Connect() == false) {
		CcLogFatal("Cannot connect to loop");
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Initializing shapes *****/
	Bars 	  bars 	   = { new Bar(SMR_INC_BAR_HEIGHT, SMR_INC_BAR_WIDTH, SMR_INC_BAR_THICK),
		      	       new Bar(SMR_INC_BAR_HEIGHT, SMR_INC_BAR_WIDTH, SMR_INC_BAR_THICK)};
	Cue*   	  cue      =   new Cue(SMR_INC_CUE_SIZE);
	Fixation* fixation =   new Fixation(SMR_INC_FIX_SIZE, SMR_INC_FIX_THICK);

	/***** Initializing engine *****/
	DrawEngine engine(wsize[0], wsize[1], 0, 0, 16, "SMR and INC - Online");
	
	/***** Setting shapes on scene *****/
	smr_inc_setup_bars(bars);
	smr_inc_setup_cue(cue);
	smr_inc_setup_fixation(fixation);

	/***** Adding shapes to engine *****/
	engine.AddShape(bars.at(0), "bar-one");
	engine.AddShape(bars.at(1), "bar-two");
	engine.AddShape(fixation,   "fixation");
	engine.AddShape(cue,        "cue");

	/***** Configuring taskset *****/
	if(taskset.Configure(xml_protocol, xml_events) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}
	for(auto it=taskset.Begin(); it!=taskset.End(); it++)
		CcLogInfoS("Threshold for taskset " << it->first << ": " << it->second.Get("threshold").AsFloat());

	/***** Configuring timings *****/
	if(timings.Configure(xml_protocol) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Configuring events *****/
	if(events.Configure(xml_events) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Configuring Trials *****/
	info = wtk_configure_trials(&taskset);
	wtk_dump_trials(&info);

	/***** Configuring Smr BCI *****/
	id.Set("smr-inc");
	id.TryAttach(idpipe);

	ic.Set("smr-inc", "");
	ic.TryAttach(icpipe, true);

	CcLogInfoS("Waiting for iC message");
	icmsg = ic.GetMessage(true);
	try {
		ic.ValidateMessage(icmsg, &taskset);
	} catch (std::runtime_error& e) {
		CcLogFatalS(e.what());
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Starting main loop *****/

	engine.Open();

	CcLogInfo("[smr-inc] Protocol starts");
	Time::Sleep(timings.Get("begin"));

	for(auto i=0; i<info.NTrials; i++) {
		
		idx = info.Sequence[i];
		CcLogInfoS("Trial " << i+1 << "/" << info.NTrials 
			   <<  " [" << idx << ":" << taskset.Get(idx)->Get("name").AsString() << "]");
		
		/***** Fixation *****/
		id.Send(events.Get("fixation"));
		cue->SetAlpha(0.0f);
		fixation->SetAlpha(1.0f);
		Time::Sleep(timings.Get("fixation"));
		id.Send(events.Get("fixation") + events.Get("off"));

		/***** Cue *****/
		id.Send(std::stoul(taskset.Get(idx)->GetEvent()));
		fixation->SetAlpha(0.0f);
		cue->SetAlpha(1.0f);
		cue->SetColor(ColorSet.at(idx));
		Time::Sleep(timings.Get("cue"));
		id.Send(std::stoul(taskset.Get(idx)->GetEvent()) + events.Get("off"));

		/***** Continous feedback *****/
		Period = wtk_random_int(timings.Get("feedback-min"), timings.Get("feedback-max"));
		F = idx == 2 ? 2.0f/Period : 1.0f/(4.0f * Period);
		A = idx == 2 ? wtk_random_float(0.2, 0.6) : 1.0f;	

		bars.at(0)->SetValue(0.0f);
		bars.at(1)->SetValue(0.0f);

		id.Send(events.Get("cfeedback"), TCBlock::BlockIdxUnset, &fidx);
		bool hit = false;	
		unsigned int idtask;
		while (hit == false) {

			if (idx == 2) {
				for(auto t=0; t<Period; t+=timings.Get("feedback-update")) {
		
					value = A*sin(2.0f *M_PI * t * F); 
					value = std::round(value*100.0f)/100.0f;
					smr_inc_update_bars(bars, value);
					Time::Sleep(timings.Get("feedback-update"));
				}
				idtask = 2;
				break;
			} else {
				do {
					icmsg = ic.GetMessage(true);
				} while(ic.SyncMessage(icmsg, fidx + 1) == false);
				//while(true) {
				//	icmsg = ic.GetMessage(true);
				//	if(icmsg->GetBlockIdx() > fidx + 1) { 
				//		printf("%d - %d\n", icmsg->GetBlockIdx(), fidx);
				//		break; 
				//	}
				//}

				ICClassifier* icc = icmsg->GetClassifier("smr-inc");
				for(auto it = icc->classes.Begin(); it != icc->classes.End(); it++) {
					idtask = taskset.Get(it->first)->GetId();
					npp = smr_inc_normalize_pp(it->second->GetValue(), taskset.Get(it->first)->Get("threshold").AsFloat(), 0.5f);
					bars.at(idtask)->SetValue(npp);
					
					if((float)it->second->GetValue() >= taskset.Get(it->first)->Get("threshold").AsFloat()) {
						bars.at(idtask)->SetAlpha(0.8f, Shape::Fill); 
						hit = true;
						break;
					}
				}
			}

		}

		id.Send(events.Get("cfeedback") + events.Get("off"));
		
		Time::Sleep(100);

		/***** Boom *****/
		if(hit & (idtask == idx)) {
			CcLogInfoS("Reached " << taskset.Get(idtask)->Get("name").AsString() <<
				   " - Target hit!");
			id.Send(events.Get("targethit"));
			Time::Sleep(timings.Get("boom"));
			id.Send(events.Get("targethit") + events.Get("off"));
		} else {
			CcLogInfoS("Reached " << taskset.Get(idtask)->Get("name").AsString() <<
				   " - Target miss!");
			id.Send(events.Get("targetmiss"));
			Time::Sleep(timings.Get("boom"));
			id.Send(events.Get("targetmiss") + events.Get("off"));
		}

		/***** Reset *****/
		bars.at(0)->SetAlpha(0.5f, Shape::Fill);
		bars.at(1)->SetAlpha(0.5f, Shape::Fill);
		bars.at(0)->SetValue(0.0f);
		bars.at(1)->SetValue(0.0f);
		fixation->SetAlpha(1.0f);
		cue->SetAlpha(0.0f);
		
		Time::Sleep(timings.Get("iti"));

		/***** Event handler *****/
		if(engine.EventKeyboard('q') | engine.EventQuit()) {
			CcLogWarning("User asked to quit");
			break;	
		}
	}
	
	CcLogInfo("[smr-inc] Protocol ends");
	Time::Sleep(timings.Get("end"));

	/***** Free memory *****/
	engine.Destroy();
	delete bars.at(0);
	delete bars.at(1);
	delete cue;
	delete fixation;
	
	CcCore::CloseLogger();
	Core::Exit(0);
}

#endif
