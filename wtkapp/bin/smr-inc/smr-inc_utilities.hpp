#ifndef SMR_INC_UTILITIES_HPP
#define SMR_INC_UTILITIES_HPP

#include <unistd.h>

#include <wtkdraw/Bar.hpp>
#include <wtkdraw/Cross.hpp>
#include <wtkdraw/Arrow.hpp>
#include <wtkdraw/Circle.hpp>
#include <wtknet/ClTobiIc.hpp>
#include <wtknet/ClTobiId.hpp>
#include <wtkcore/Time.hpp>

#include <vector>
#include <drawtk.h>

#define SMR_INC_WIN_WIDTH	1024
#define SMR_INC_WIN_HEIGHT 	768

#define SMR_INC_BAR_WIDTH 	0.3f
#define SMR_INC_BAR_HEIGHT 	0.6f
#define SMR_INC_BAR_THICK 	0.02f

#define SMR_INC_FIX_SIZE	0.2f
#define SMR_INC_FIX_THICK  	0.03f

#define SMR_INC_CUE_SIZE  	0.1f

#define SMR_INC_AMPLITUDE 	0.6f

#define SMR_INC_XML_EVENTS 	"/home/ltonin/.whitk/xml/wtk_events.xml"
#define SMR_INC_XML_TASKS	"/home/ltonin/.whitk/xml/wtk_tasks.xml"
#define SMR_INC_XML_LOOP	"/home/ltonin/.whitk/xml/wtk_smr_loop.xml"

using namespace wtk::draw;

typedef Circle  Cue;
typedef Cross   Fixation;
typedef std::vector<Bar*> Bars;
const std::vector<const float*> ColorSet = {dtk_plum_dark, dtk_skyblue_dark, dtk_chameleon_med};

typedef wtk::core::Container<std::string, std::string> Options;
typedef wtk::core::Container<std::string, unsigned int> ClassMap;

void smr_inc_setup_bars(Bars& bars) {
	
	bars.at(0)->Move(-0.5f, 0.0f);
	bars.at(0)->SetColor(ColorSet.at(0));
	bars.at(0)->SetAlpha(0.5f, Shape::Fill);
	bars.at(0)->SetValue(0.0f);
	
	bars.at(1)->Move(0.5f, 0.0f);
	bars.at(1)->SetColor(ColorSet.at(1));
	bars.at(1)->SetAlpha(0.5f, Shape::Fill);
	bars.at(1)->SetValue(0.0f);
}

void smr_inc_setup_fixation(Fixation* fixation) {
	fixation->Move(0.0f, 0.0f);
	fixation->SetColor(dtk_white);
}

void smr_inc_setup_cue(Cue* cue) {
	cue->Move(0.0f, 0.0f);
	cue->SetColor(dtk_white);
	cue->SetAlpha(0.0f);
}

void smr_inc_update_bars(Bars& bars, float value) {
	
	float cvalue[2] = {0.0f, 0.0f}; 	
	auto bId = value <= 0.0f ? 1 : 0;
	cvalue[bId] = std::abs(value);

	bars.at(0)->SetValue(cvalue[0]);
	bars.at(1)->SetValue(cvalue[1]);
}

float smr_inc_normalize_pp(float value, float max, float min) {
	float nvalue;
	nvalue = ((1.0f - 0.0f) * (value - min))/(max - min) + 0.0f;
	nvalue = nvalue >= 1.0f ? 1.0f : nvalue;
	nvalue = nvalue < 0.0f ? 0.0f : nvalue;
	return nvalue;
}


bool smr_inc_getopts(Options* opt, int narg, char** varg) {
	
	int c;
	bool result = false;
	while ((c = getopt(narg, varg, "x:e:f")) != -1) {
		switch(c) {
			case 'x':
				opt->Set("xml-protocol", optarg);
				result = true;
				break;
			case 'e':
				opt->Set("xml-events", optarg);
				break;
			case 'f':
				opt->Set("win-w", "0");
				opt->Set("win-h", "0");
				break;
			default:
				break;
		}
	}

	opt->Set("xml-tasks",  SMR_INC_XML_TASKS);
	opt->Set("xml-events", SMR_INC_XML_EVENTS);
	opt->Set("win-w",      std::to_string(SMR_INC_WIN_WIDTH));
	opt->Set("win-h",      std::to_string(SMR_INC_WIN_HEIGHT));

	return result;
};



#endif
