#ifndef SMR_INC_OFFLINE_CPP
#define SMR_INC_OFFLINE_CPP

#include <stdlib.h> 
#include <cmath>
#include <drawtk.h>

#include <cnbiloop/ClLoop.hpp>
#include <cnbicore/CcLog.hpp>

#include <wtkcore/Time.hpp>
#include <wtkcore/wtk_random_utilities.hpp>
#include <wtkdraw/Bar.hpp>
#include <wtkdraw/Cross.hpp>
#include <wtkdraw/Circle.hpp>
#include <wtkdraw/DrawEngine.hpp>

#include <wtknet/IFaceD.hpp>

#include <wtkprotocol/TaskSet.hpp>
#include <wtkprotocol/Timings.hpp>
#include <wtkprotocol/Events.hpp>
#include <wtkprotocol/wtk_protocol_utilities.hpp>

#include "smr-inc_utilities.hpp"

using namespace std;
using namespace wtk::core;
using namespace wtk::draw;
using namespace wtk::protocol;
using namespace wtk::net;


void usage(void) {
	printf("\nUsage: smr-inc_offline -x XML_PROTOCOL [OPTIONS]...\n");
	printf("\n Options:");
	printf("\n  -e	XML_EVENTS	Path to xml file with events 	[Default: $HOME/.whitk/xml/wtk_events.xml]");
	printf("\n  -b	BUSNAME		iD bus name 		[Default: '/bus']");
	printf("\n  -f			Fullscreen 		[Default: 1024x768]");
	printf("\n\n");
}

int main (int argc, char** argv) {

	int opt;
	std::string xml_protocol;
	std::string xml_events = SMR_INC_XML_EVENTS;
	unsigned int wsize[2]  = {SMR_INC_WIN_WIDTH, SMR_INC_WIN_HEIGHT};
	std::string idpipe = "/bus";

	while((opt = getopt(argc, argv, "x:e:p:f")) != -1) {
		switch(opt) {
			case 'x':
				xml_protocol.assign(optarg);
				break;
			case 'e':
				xml_events.assign(optarg);
				break;
			case 'b':
				idpipe.assign(optarg);
				break;
			case 'f':
				wsize[0] = 0;
				wsize[1] = 0;
				break;
			default:
				usage();
				return -1;
		}
	}

	unsigned int idx;
	float Period;
	float F;
	float A;
	float value;

	IFaceD  id(IFaceD::AsSender);
	TaskSet taskset;	
	Timings timings;
	Events  events;
	TrialInfo info;

	/***** CNBI Loop setup *****/
	CcCore::OpenLogger("smr-inc_offline");
	ClLoop::Configure();
	if(ClLoop::Connect() == false) {
		CcLogFatal("Cannot connect to loop");
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Initializing shapes *****/
	Bars 	  bars 	   = { new Bar(SMR_INC_BAR_HEIGHT, SMR_INC_BAR_WIDTH, SMR_INC_BAR_THICK),
		      	       new Bar(SMR_INC_BAR_HEIGHT, SMR_INC_BAR_WIDTH, SMR_INC_BAR_THICK)};
	Cue*   	  cue      =   new Cue(SMR_INC_CUE_SIZE);
	Fixation* fixation =   new Fixation(SMR_INC_FIX_SIZE, SMR_INC_FIX_THICK);

	/***** Initializing engine *****/
	DrawEngine engine(wsize[0], wsize[1], 0, 0, 16, "SMR and INC - Offline");
	
	/***** Setting shapes on scene *****/
	smr_inc_setup_bars(bars);
	smr_inc_setup_cue(cue);
	smr_inc_setup_fixation(fixation);

	/***** Adding shapes to engine *****/
	engine.AddShape(bars.at(0), "bar-one");
	engine.AddShape(bars.at(1), "bar-two");
	engine.AddShape(fixation,   "fixation");
	engine.AddShape(cue,        "cue");

	/***** Configuring taskset smr *****/
	if(taskset.Configure(xml_protocol, xml_events) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Configuring timings *****/
	if(timings.Configure(xml_protocol) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Configuring events *****/
	if(events.Configure(xml_events) == 0) {
		CcCore::CloseLogger();
		Core::Exit(0);
	}

	/***** Configuring Trials *****/
	info = wtk_configure_trials(&taskset);
	wtk_dump_trials(&info);

	/***** Configuring Smr BCI *****/
	id.Set("smr-inc");
	id.TryAttach(idpipe);

	/***** Starting main loop *****/

	engine.Open();

	CcLogInfo("[smr-inc] Protocol starts");
	Time::Sleep(timings.Get("begin"));

	for(auto i=0; i<info.NTrials; i++) {
		
		idx = info.Sequence[i];
		CcLogInfoS("Trial " << i+1  << "/" << info.NTrials 
			   << " [" << idx << ":" << taskset.Get(idx)->Get("name").AsString() << "]");  

		/***** Fixation *****/
		id.Send(events.Get("fixation"));
		cue->SetAlpha(0.0f);
		fixation->SetAlpha(1.0f);
		Time::Sleep(timings.Get("fixation"));
		id.Send(events.Get("fixation") + events.Get("off"));

		/***** Cue *****/
		id.Send(std::stoul(taskset.Get(idx)->GetEvent()));
		fixation->SetAlpha(0.0f);
		cue->SetAlpha(1.0f);
		cue->SetColor(ColorSet.at(idx));
		Time::Sleep(timings.Get("cue"));
		id.Send(std::stoul(taskset.Get(idx)->GetEvent()) + events.Get("off"));

		/***** Continous feedback *****/
		Period = wtk_random_int(timings.Get("feedback-min"), timings.Get("feedback-max"));
		F = idx == 2 ? 2.0f/Period : 1.0f/(4.0f * Period);
		A = idx == 2 ? wtk_random_float(0.2, 0.6) : 1.0f;	

		id.Send(events.Get("cfeedback"));
		for(auto t=0; t<Period; t+=timings.Get("feedback-update")) {
		
			value = A*sin(2.0f *M_PI * t * F + M_PI*idx); 
			value = std::round(value*100.0f)/100.0f;
			smr_inc_update_bars(bars, value);

			Time::Sleep(timings.Get("feedback-update"));
		}
		id.Send(events.Get("cfeedback") + events.Get("off"));
		
		/***** Boom *****/
		if(idx != 2) {
			bars.at(idx)->SetAlpha(0.8f, Shape::Fill); 
		}

		Time::Sleep(timings.Get("boom"));

		/***** Reset *****/
		bars.at(0)->SetAlpha(0.5f, Shape::Fill);
		bars.at(1)->SetAlpha(0.5f, Shape::Fill);
		bars.at(0)->SetValue(0.0f);
		bars.at(1)->SetValue(0.0f);
		fixation->SetAlpha(1.0f);
		cue->SetAlpha(0.0f);
		
		Time::Sleep(timings.Get("iti"));

		/***** Event handler *****/
		if(engine.EventKeyboard('q') | engine.EventQuit()) {
			CcLogWarning("User asked to quit");
			break;	
		}
	}
	
	CcLogInfo("[smr-inc] Protocol ends");
	Time::Sleep(timings.Get("end"));

	/***** Free memory *****/
	engine.Destroy();
	delete bars.at(0);
	delete bars.at(1);
	delete cue;
	delete fixation;
	
	CcCore::CloseLogger();
	Core::Exit(0);
}

#endif
