#ifndef SMR_PROC_DEBUG_CPP
#define SMR_PROC_DEBUG_CPP

#include "Debug.hpp"

namespace wtk {
	namespace app {

Debug::Debug(std::ios_base::openmode mode) {
	this->mode_     = mode;
	this->filename_ = "";
	this->line_     = "";
	this->enable_   = false;
}

Debug::~Debug(void) {
	if(this->file_.is_open())
		this->file_.close();

	if(this->IsRunning()) {
		this->Stop();
	}
}

void Debug::Open(std::string name) {
	this->filename_ = this->get_current_date() + "_" + name + ".txt";
	
	this->file_.open(this->filename_.c_str(), this->mode_); 

	if(this->file_.is_open()) {
		this->Start();	
		this->sem_.Wait();
	} else {
		throw std::runtime_error("Cannot open debug file at: " + this->filename_);
	}
	
	printf("Debug class instaciated\n");
}

std::string Debug::GetFilename(void) {
	return this->filename_;
}

void Debug::Enable(void) {
	this->enable_ = true;
}

void Debug::Disable(void) {
	this->enable_ = false;
}

void Debug::Main(void) {
	
	wtk::core::TimeValue tic;
	unsigned int i =0;
	while(this->IsRunning() & this->file_.is_open()) {
	
		this->sem_.Wait();
		if(this->enable_ == true) 
			this->file_ << this->line_ << std::endl;
		this->line_.clear();

	}
}

void Debug::Write(void) {
	this->sem_.Post();
}

void Debug::AddEntry(std::string entry) {
	this->line_ = this->line_ + " " + entry;
}


std::string Debug::get_current_date(void) {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y%m%d%H%M%S", &tstruct);

    return buf;
}
	}
}

#endif

