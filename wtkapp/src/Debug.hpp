#ifndef SMR_PROC_DEBUG_HPP
#define SMR_PROC_DEBUG_HPP

#include <string>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <wtkcore/Semaphore.hpp>
#include <wtkcore/Thread.hpp>
#include <wtkcore/Time.hpp>

namespace wtk {
	namespace app {

class Debug : public wtk::core::Thread {

	public:
		Debug(std::ios_base::openmode mode = (std::ofstream::out | std::ofstream::trunc));
		~Debug(void);
		
		void Enable(void);
		void Disable(void);

		void Open(std::string filename);
		std::string GetFilename(void);
		void Main(void);
		void AddEntry(std::string entry);
		void Write(void);
	protected:
		std::string get_current_date(void);	

	protected:
		bool 				enable_;
		std::ofstream 			file_;
		std::string 			filename_;
		std::string 			line_;
		std::ios_base::openmode 	mode_;
		wtk::core::Semaphore 		sem_;

};

	}
}


#endif

