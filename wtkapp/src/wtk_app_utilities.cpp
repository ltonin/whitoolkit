#ifndef WTK_APP_UTILITIES_CPP
#define WTK_APP_UTILITIES_CPP

#include "wtk_app_utilities.hpp"

namespace wtk {
	namespace app {

std::string wtk_get_datetime(void) {
	return wtk_get_date() + wtk_get_time();
}

std::string wtk_get_date(void) {
	time_t     now = time(0);
   	struct tm  tstruct;
    	char       buf[80];
    	tstruct = *localtime(&now);
    	strftime(buf, sizeof(buf), "%Y%m%d", &tstruct);

    	return buf;
}

std::string wtk_get_time(void) {
	time_t     now = time(0);
   	struct tm  tstruct;
    	char       buf[80];
    	tstruct = *localtime(&now);
    	strftime(buf, sizeof(buf), "%H%M%S", &tstruct);

    	return buf;
}

std::string wtk_get_xmlfield(std::string xml, std::string root, std::string node) {

	wtk::xml::XReader reader(root);
	std::string field = "";

	try {
		reader.ImportFileEx(xml);
	} catch (wtk::xml::XException& e) {
		throw std::runtime_error(e.Info() + " in " + xml);
	}	

	try {
		field = reader.RootEx()->QuickEx(node)->GetRawEx();
	} catch (wtk::xml::XException& e) {
		throw std::runtime_error(e.Info());
	}	

	return field;
}

//void wtk_progress_bar(unsigned int x, unsigned int n, unsigned int w)
//{
//    if ( (x != n) && (x % (n/100+1) != 0) ) return;
// 
//    float ratio  =  x/(float)n;
//    int   c      =  ratio * w;
// 
//    std::cout << std::setw(3) << (int)(ratio*100 + 1) << "% [";
//    for (int x=0; x<c; x++) std::cout << "=";
//    for (int x=c; x<w; x++) std::cout << " ";
//    std::cout << "]\r" << std::flush;
//}

	}
}

#endif

