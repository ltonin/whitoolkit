#ifndef WTK_APP_UTILITIES_HPP
#define WTK_APP_UTILITIES_HPP

#include <string>
#include <iomanip>
#include <stdexcept>

#include <wtkxml/XReader.hpp>

#define WTKOUT_RESET   	"\033[0m"
#define WTKOUT_BLACK   	"\033[30m"      /* Black */
#define WTKOUT_RED     	"\033[31m"      /* Red */
#define WTKOUT_GREEN   	"\033[32m"      /* Green */
#define WTKOUT_YELLOW  	"\033[33m"      /* Yellow */
#define WTKOUT_BLUE    	"\033[34m"      /* Blue */
#define WTKOUT_MAGENTA 	"\033[35m"      /* Magenta */
#define WTKOUT_CYAN    	"\033[36m"      /* Cyan */
#define WTKOUT_WHITE   	"\033[37m"      /* White */
#define WTKOUT_BOLD   	"\033[1m"      	/* Bold */

namespace wtk {
	namespace app {

std::string wtk_get_date(void);
std::string wtk_get_time(void);
std::string wtk_get_datetime(void);
std::string wtk_get_xmlfield(std::string xml, std::string root, std::string node);
static inline void wtk_progress_bar(unsigned int x, unsigned int n, unsigned int w = 50) {
    if ( (x != n) && (x % (n/100+1) != 0) ) return;

    if ( x == n ) { std::cout<<std::endl; return; }
 
    float ratio  =  x/(float)n;
    int   c      =  ratio * w;
 
    std::cout << std::setw(3) << (int)(ratio*100 + 1) << "% [";
    for (int x=0; x<c; x++) std::cout << "=";
    for (int x=c; x<w; x++) std::cout << " ";
    std::cout << "]\r" << std::flush;
}


	}
}


#endif
