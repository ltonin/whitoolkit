#ifndef SMR_BCI_CPP
#define SMR_BCI_CPP

#include "SmrBci.hpp"

namespace wtk {
	namespace app {

SmrBci::SmrBci(wtk::protocol::TaskSet* taskset, std::string xmlbci) {

	ssize_t asize = 0;
	this->taskset_ = taskset;

	// Configure BCI parameters from xml
	if(xmlconfig_from_bci(xmlbci) == false) {
		throw std::runtime_error("Bci configuration failed");
	}

	// Connection to the loop
	if(ClLoop::nms.Query(this->parameters_.Get("pipe-name").AsString(), &this->pipe_) != ClNmsLang::Successful)
		throw std::runtime_error("Cannot query pipe");

	// Try to open pipe reader
	if(this->reader_.Open(this->pipe_, CcPipe::Reader) == false)
		throw std::runtime_error("Cannot open pipe");
	
	// Try to read acknowledgment message
	asize = this->reader_.TryRead(this->ack_.buffer, NDF_ACK_SIZET);
	if(asize <= 0)
		throw std::runtime_error("Pipe is broken");
	
	this->ack_.bsize = asize;

	if(this->ack_.bsize != NDF_ACK_SIZET)
		throw std::runtime_error("Acknoledgement not received correctly");
	
	// Initialize ndf_frame
	ndf_initack(&this->frame_, &this->ack_);
}

SmrBci::~SmrBci(void) {
	this->reader_.Close();
	ndf_free(&this->frame_);
}

bool SmrBci::Setup(std::string classifierpath) {
	
	// Setup ndf frame and raw data array
	this->data_   = (float*)malloc(sizeof(float)*this->frame_.config.samples*this->frame_.config.eeg_channels);	

	this->framerate_ = 1000.0f*(float)this->frame_.config.samples/(float)this->frame_.config.sf;

	// Setup Ring buffer
	this->buffer_ = new wtk::proc::RingBuffer();
	this->buffer_->Setup(this->parameters_.Get("buffer-size").AsUInt(), this->frame_.config.eeg_channels);
	
	// Setup Laplacian filter
	try {
		this->laplacian_ = new wtk::proc::Laplacian();
		this->laplacian_->Setup(this->parameters_.Get("lap-path").AsString().c_str());
	} catch (std::runtime_error& e) {
		throw std::runtime_error("Can't setup Laplacian filter because: '" + std::string(e.what()) + "'");
	}

	// Setup Pwelch method
	this->psd_ = new wtk::proc::Pwelch();
	this->psd_->Setup(this->parameters_.Get("psd-wlength").AsUInt(), 
			  wtk::proc::Window::AsHamming, 
			  this->parameters_.Get("psd-novl").AsUInt(), 
			  this->frame_.config.sf, 
			  this->parameters_.Get("psd-dolog").AsUInt());
	
	// Setup Gaussian classifier
	try {
		this->classifier_ = new wtk::proc::Gaussian();
		this->classifier_->Setup(classifierpath.c_str());
	} catch (std::runtime_error& e) {
		throw std::runtime_error("Can't setup classifier because: '" + std::string(e.what()) + "'");
	}

	// Compare class in classifier with provided taskset
	for(auto it = this->classifier_->config.classlbs.begin(); it != this->classifier_->config.classlbs.end(); it++) {
		if(this->taskset_->Has(std::to_string(*it)) == false) {
			throw std::runtime_error("Provided taskset does not have task " 
			      			+ std::to_string(*it) 
			      			+ " requested by classifier from " 
			      			+ classifierpath);
		}
	}
	
	// Setup rejection
	this->rejection_ = SMR_DEFAULT_REJECTION;
	
	// Setup Integrator
	this->integrator_ = new wtk::proc::Exponential(SMR_DEFAULT_INTEGRATION, this->classifier_->config.nclasses);
	

	// Setup temporary data matrices
	this->dmap_ = Eigen::MatrixXf::Zero(this->frame_.config.eeg_channels, this->frame_.config.samples);
	this->dbuf_ = Eigen::MatrixXd::Zero(this->parameters_.Get("buffer-size").AsUInt(), this->frame_.config.eeg_channels);
	this->dlap_ = Eigen::MatrixXd::Zero(this->parameters_.Get("buffer-size").AsUInt(), this->frame_.config.eeg_channels);
	this->dfet_ = Eigen::VectorXd::Zero(this->classifier_->config.nfeatures);

	this->rawpp_ = Eigen::VectorXd::Zero(this->classifier_->config.nclasses);
	this->intpp_ = Eigen::VectorXd::Zero(this->classifier_->config.nclasses);
	
}


bool SmrBci::xmlconfig_from_bci(std::string xmlbci) {
	
	wtk::xml::XNode   	node;
	wtk::xml::XReader	reader("wtkbci");
	
	// Try to read xml file for wtkbci
	try {
		reader.ImportFileEx(xmlbci);
	} catch (wtk::xml::XException& e) {
		throw std::runtime_error(e.Info() + " in " + xmlbci);
	}	

	reader.RootEx()->SetBranch();
	
	// Look for pipe-name node
	try {
		this->pipe_ = reader.BranchEx()->QuickStringEx("pipe-name");
	} catch (wtk::xml::XException& e) {
		throw std::runtime_error(e.Info() + " in " + xmlbci);
	}	

	// Iterate on xml nodes
	reader.RootEx()->SetBranch();
	node = reader.Child();

	while(node != NULL) {
		this->parameters_.Set(node->name(), wtk::core::Item(node->value()));
		node = reader.NextSibling();
	}
	
	return true;
}

float SmrBci::GetFrameRate(void) {
	return this->framerate_;
}

void SmrBci::SetRejection(float value) {
	CcLogInfoS("Rejection value set at " << value);
	this->rejection_ = value;
}

void SmrBci::SetIntegration(float value) {
	CcLogInfoS("Integration value set at " << value);
	this->integrator_->SetAlpha(value);
}

void SmrBci::Reset(void) {
	this->integrator_->Reset();
	this->intpp_.fill(0.5);
	for(auto it = this->classifier_->config.classlbs.begin(); it != this->classifier_->config.classlbs.end(); it++) {
		pp.Set(std::to_string(*it), 0.5f);
		ipp.Set(std::to_string(*it), 0.5f);
	}
}

unsigned int SmrBci::GetFrameIdx(void) {
	return this->fidx_;
}

bool SmrBci::Classify(void) {

	size_t rsize;
	unsigned int i;
	Probability pp;
	Probability ipp;

	// Try to read data frame
	rsize = this->reader_.TryRead(this->frame_.data.buffer, this->frame_.data.bsize);
	
	if(this->frame_.data.bsize != rsize)
		throw std::runtime_error("Pipe is broken");

	ndf_read_eeg_frame(&this->frame_, this->data_); 
	this->fidx_ = ndf_get_fidx(&this->frame_);

	// Copy data in eigen structure
	this->dmap_ = Eigen::Map<Eigen::MatrixXf>(this->data_, this->frame_.config.eeg_channels, this->frame_.config.samples);
	this->dmap_.transposeInPlace();

	this->buffer_->Add(this->dmap_.cast<double>());

	if(this->buffer_->IsFull() == false)
		return false;

	this->buffer_->Get(this->dbuf_);
	this->laplacian_->Apply(this->dbuf_, this->dlap_);
	this->psd_->Apply(this->dlap_);

	this->psd_->Get(this->dfet_, this->classifier_->config.idchan, this->classifier_->config.idfreq);
	this->classifier_->Run(this->dfet_, this->rawpp_);

	// If raw pp are greater then rejection, apply integration
	// otherwise send old probabilities
	if(this->rawpp_.maxCoeff() > this->rejection_) {
		// Apply integration
		this->integrator_->Apply(this->rawpp_, this->intpp_);
	} 


	i = 0;
	for(auto it = this->classifier_->config.classlbs.begin(); it != this->classifier_->config.classlbs.end(); it++) {
		pp.Set(std::to_string(*it), this->rawpp_(i));
		ipp.Set(std::to_string(*it), this->intpp_(i));
		i++;
	}

	this->pp  = pp;
	this->ipp = ipp;

	return true;

}

void SmrBci::Dump(void) {

	printf("[SmrBci::Dump] Bci configuration:\n");
	for(auto it = this->parameters_.Begin(); it != this->parameters_.End(); it++) {
		printf("	|-%s		%s\n", it->first.c_str(), it->second.AsString().c_str());
	}
}


	}
}


#endif
