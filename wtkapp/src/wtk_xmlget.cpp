#ifndef WTK_XMLGET_CPP
#define WTK_XMLGET_CPP

#include <unistd.h> 

#include "wtk_app_utilities.hpp"

using namespace std;
using namespace wtk::app;

void usage(void) {
	printf("\nUsage: wtk_xmlget -x XML_PROTOCOL -r XMLROOT -p XMLNODE\n");
	printf("\n\n");
}

int main(int argc, char** argv) {

	int opt;
	bool mandatory = false;
	std::string xmlprotocol;
	std::string xmlroot;
	std::string xmlnode;
	std::string field;

	while((opt = getopt(argc, argv, "x:r:p:")) != -1) {
		switch(opt) {
			case 'x':
				xmlprotocol.assign(optarg);
				break;
			case 'r':
				xmlroot.assign(optarg);
				break;
			case 'p':
				xmlnode.assign(optarg);
				break;
			default:
				usage();
				return -1;
		}
	}

	if (argc != 7) {
		usage();
		return -1;
	}


	try {
		field = wtk_get_xmlfield(xmlprotocol, xmlroot, xmlnode);
		printf("%s", field.c_str());
	} catch (runtime_error& e) {
		printf("%s\n", e.what());
	}

	return 0;
}

#endif
