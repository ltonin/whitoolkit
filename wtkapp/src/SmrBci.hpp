#ifndef SMR_BCI_HPP
#define SMR_BCI_HPP

#include <string>
#include <ndf/ndf_codec.h>
#include <cnbicore/CcPipe.hpp>

#include <cnbiloop/ClLoop.hpp>

#include <wtkcore/Item.hpp>
#include <wtkcore/Container.hpp>
#include <wtkxml/XReader.hpp>
#include <wtkprotocol/TaskSet.hpp>
#include <wtkprocessing/RingBuffer.hpp>
#include <wtkprocessing/Laplacian.hpp>
#include <wtkprocessing/Pwelch.hpp>
#include <wtkprocessing/Gaussian.hpp>
#include <wtkprocessing/Exponential.hpp>

#define SMR_DEFAULT_INTEGRATION 	0.96f
#define SMR_DEFAULT_REJECTION 		0.55f

namespace wtk {
	namespace app {

typedef wtk::core::Container<std::string, wtk::core::Item> SmrParameters;
typedef wtk::core::Container<std::string, double> Probability;

class SmrBci {

	public:

		SmrBci(wtk::protocol::TaskSet* taskset, std::string xmlbci);
		~SmrBci(void);
		
		bool Setup(std::string classifierpath);
		void SetRejection(float value);
		void SetIntegration(float value);
		float GetFrameRate(void);
		unsigned int GetFrameIdx(void);
		bool Classify(void);
		void Reset(void);
		void Dump(void);
		
	protected:
		bool xmlconfig_from_bci(std::string xmlbci);

	public:
		Probability 	pp;
		Probability 	ipp;
	

	protected:
		CcPipe 		reader_;
		std::string 	pipe_;
		ndf_frame	frame_;
		unsigned int	fidx_;
		ndf_ack		ack_;
		float* 		data_;
		float 		framerate_;


		float		rejection_;
		float 		integration_;
	
		Eigen::MatrixXf dmap_;
		Eigen::MatrixXd dbuf_;
		Eigen::MatrixXd dlap_;
		Eigen::VectorXd dfet_;
	
		Eigen::VectorXd rawpp_;
		Eigen::VectorXd intpp_;
	
		SmrParameters 	parameters_;
		wtk::protocol::TaskSet* taskset_;
		wtk::proc::RingBuffer* 	buffer_;
		wtk::proc::Laplacian*	laplacian_;
		wtk::proc::Pwelch* 	psd_;
		wtk::proc::Gaussian* 	classifier_;
		wtk::proc::Exponential*	integrator_;


};

	}
}


#endif
