#include <wtkcore/Time.hpp>
#include "Debug.hpp"


using namespace std;
using namespace wtk::core;
using namespace wtk::app;

int main(int arcg, char** argv) {

	
	Debug dbg;

	try {
		dbg.Open("debug-test");
	} catch (runtime_error& e) {
		printf("%s\n", e.what());
		exit(0);
	}

	for(auto i = 0; i<10; i++) {
		printf("Cycle: %d\n", i);
		dbg.AddEntry("a");
		dbg.AddEntry("b");
		dbg.AddEntry("c");
		dbg.Write();

		Time::Sleep(1000);
	}

	return 0;
}
